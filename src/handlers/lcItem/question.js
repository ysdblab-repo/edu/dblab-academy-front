import deepCopy from 'deep-copy';
import LcItemHandler from './index';
import questionService from '../../services/questionService';
import fileService from '../../services/fileService';
import utils from '../../utils';
import { consoleTestResultHandler } from 'tslint/lib/test';

export default class QuestionHandler extends LcItemHandler {
  // static postLcItem() {
  //   super.postLcItem();
  //   console.log('QuestionHandler postLcItem');
  // }
  /* eslint-disable no-param-reassign */
  static async initViewModel(vm) {
    const item = vm.lectureItem;
    const q = item.questions[0];

    vm.$set(vm.inputTail, 'question', q.question);
    vm.inputTail.questionMediaType = q.question_media_type;
    if (vm.inputTail.questionMediaType === 'youtube') {
      vm.$set(vm.inputTail, 'questionYoutubeURL', q.question_media);
    } else if (vm.inputTail.questionMediaType === 'voice') {
      const res = await fileService.getFile({ guid: q.question_media });
      const blob = new Blob([res.data], { type: 'audio/webm' });
      const bolbURL = URL.createObjectURL(blob);
      vm.$set(vm.inputTail, 'questionBlobURL', bolbURL);
    }
    vm.inputTail.difficulty = q.difficulty;
    const keywordList = await questionService.getQuestionKeywords({
      questionId: q.question_id,
    });
    keywordList.data.forEach((element) => {
      element.score = element.score_portion;
    });
    
    vm.inputTail.assignedKeywordList = keywordList.data;

    
    switch (q.type) {
      case 1: { // 단답
        vm.inputBody.questionType = 'SHORT_ANSWER';
        vm.$set(vm.inputTail, 'answer', q.answer);
        break;
      }
      case 0: { // 객관
        vm.inputBody.questionType = 'MULTIPLE_CHOICE';
        q.answer = q.answer.map(element => Number(element));
        vm.$set(vm.inputTail, 'answer', q.answer);
        vm.$set(vm.inputTail, 'multiChoiceMediaType', q.multi_choice_media_type);
        vm.$set(vm.inputTail, 'questionList', q.choice);

        if (q.multi_choice_media_type === 'image') {
          const ress = [];
          for (const [index, clientPath] of q.choice.entries()) {
            if (clientPath !== '') {
              const res = await fileService.getFileName({ clientPath });
              ress.push(res);
            } else {
              ress.push(null);
            }
          }

          if (ress[0] !== null) {
            vm.$refs.questionEditor.$refs.MultiChoiceUpload0[0].uploadFiles.push(ress[0].data);
          }
          if (ress[1] !== null) {
            vm.$refs.questionEditor.$refs.MultiChoiceUpload1[0].uploadFiles.push(ress[1].data);
          }
          if (ress[2] !== null) {
            vm.$refs.questionEditor.$refs.MultiChoiceUpload2[0].uploadFiles.push(ress[2].data);
          }
          if (ress[3] !== null) {
            vm.$refs.questionEditor.$refs.MultiChoiceUpload3[0].uploadFiles.push(ress[3].data);
          }
          if (ress[4] !== null) {
            vm.$refs.questionEditor.$refs.MultiChoiceUpload4[0].uploadFiles.push(ress[4].data);
          }
          /*for (const [index, clientPath] of q.choice.entries()) {
            if (clientPath !== '') {
              const res = await fileService.getFileName({ clientPath });
              eval(`vm.$refs.questionEditor.$refs.MultiChoiceUpload${index}[0].uploadFiles.push(res.data)`); // el-upload 컴포넌트에 보이도록 추가
            }
          }*/
        } else if (q.multi_choice_media_type === 'voice') {
          const bolbURLList = [];
          for (const [index, guid] of q.choice.entries()) {
            if (guid !== '') {
              const res = await fileService.getFile({ guid });
              const blob = new Blob([res.data], { type: 'audio/webm' });
              bolbURLList[index] = URL.createObjectURL(blob);
            }
          }
          vm.$set(vm.inputTail, 'blobURL', bolbURLList);
        }

        break;
      }
      case 2: { // 서술
        vm.inputBody.questionType = 'DESCRIPTION';
        vm.$set(vm.inputTail, 'answer', q.answer);
        vm.$nextTick(() => {
          vm.$set(vm.inputTail, 'answerFile', vm.$refs.questionEditor.$refs.answerUpload.uploadFiles);
          if (q.files.length !== 0) {
            for (let i = 0; i < q.files.length; i += 1) {
              vm.$refs.questionEditor.$refs.answerUpload.uploadFiles.push({
                name: q.files[i].name,
                file_guid: q.files[i].file_guid,
              });
            }
            // 문항 유형 변경시에도 기존 파일 목록 유지하기 위해 inputBody에 삽입.
            vm.$set(vm.inputBody, 'oldAnswerFile', q.files);
          }
        });
        break;
      }
      case 3: { // SW
        vm.inputBody.questionType = 'SW';
        vm.$set(vm.inputTail, 'language', q.accept_language[0]);
        vm.$set(vm.inputTail, 'inputDescription', q.input_description);
        vm.$set(vm.inputTail, 'outputDescription', q.output_description);
        vm.$set(vm.inputTail, 'sampleInput', q.sample_input);
        vm.$set(vm.inputTail, 'sampleOutput', q.sample_output);
        vm.$set(vm.inputTail, 'memoryLimit', q.memory_limit);
        vm.$set(vm.inputTail, 'timeLimit', q.time_limit);
        vm.$set(vm.inputTail, 'code', q.sample_code);
        vm.$set(vm.inputTail, 'testCaseList', q.problem_testcases);
        vm.$set(vm.inputTail, 'testCaseListOld', deepCopy(q.problem_testcases));
        break;
      }
      case 4: { // SQL
        vm.inputBody.questionType = 'SQL';
        vm.$set(vm.inputTail, 'answer', q.answer);
        vm.$nextTick(() => {
          vm.$set(vm.inputTail, 'sqlFile', vm.$refs.questionEditor.$refs.sqlUpload.uploadFiles);
          if (q.sql_lite_file[0] !== undefined) {
            vm.$refs.questionEditor.$refs.sqlUpload.uploadFiles.push({
              name: q.sql_lite_file[0].name,
              file_guid: q.sql_lite_file[0].file_guid,
            });
            // 문항 유형 변경시에도 기존 파일 목록 유지하기 위해 inputBody에 삽입.
            vm.$set(vm.inputBody, 'sqlFileOld', {
              name: q.sql_lite_file[0].name,
              file_guid: q.sql_lite_file[0].file_guid,
            });
          }
        });
        break;
      }
      case 5: { // VIDEO
        vm.inputBody.questionType = 'VIDEO';
        vm.$set(vm.inputTail, 'answer', q.answer);
        break;
      }
      case 6: { // MULTIMEDIA
        vm.inputBody.questionType = 'MULTIMEDIA';
        vm.$set(vm.inputTail, 'answerMediaType', q.answer_media_type);
        break;
      }
      case 7: { // CONNECTION
        vm.inputBody.questionType = 'CONNECTION';
        q.answer = q.answer.map(element => Number(element));
        vm.$set(vm.inputTail, 'answer', q.answer);
        vm.$set(vm.inputTail, 'multiChoiceMediaType', q.multi_choice_media_type);
        vm.$set(vm.inputTail, 'questionList', q.choice);
        vm.$set(vm.inputTail, 'choice2', q.choice2);

        if (q.multi_choice_media_type === 'image') {
          const ress = [];
          for (const [index, clientPath] of q.choice.entries()) {
            if (clientPath !== '') {
              const res = await fileService.getFileName({ clientPath });
              ress.push(res);
            } else {
              ress.push(null);
            }
          }

          if (ress[0] !== null) {
            vm.$refs.questionEditor.$refs.MultiChoiceUpload0[0].uploadFiles.push(ress[0].data);
          }
          if (ress[1] !== null) {
            vm.$refs.questionEditor.$refs.MultiChoiceUpload1[0].uploadFiles.push(ress[1].data);
          }
          if (ress[2] !== null) {
            vm.$refs.questionEditor.$refs.MultiChoiceUpload2[0].uploadFiles.push(ress[2].data);
          }
          if (ress[3] !== null) {
            vm.$refs.questionEditor.$refs.MultiChoiceUpload3[0].uploadFiles.push(ress[3].data);
          }
          if (ress[4] !== null) {
            vm.$refs.questionEditor.$refs.MultiChoiceUpload4[0].uploadFiles.push(ress[4].data);
          }
          /*for (const [index, clientPath] of q.choice.entries()) {
            if (clientPath !== '') {
              const res = await fileService.getFileName({ clientPath });
              eval(`vm.$refs.questionEditor.$refs.MultiChoiceUpload${index}[0].uploadFiles.push(res.data)`); // el-upload 컴포넌트에 보이도록 추가
            }
          }*/
        } else if (q.multi_choice_media_type === 'voice') {
          const bolbURLList = [];
          for (const [index, guid] of q.choice.entries()) {
            if (guid !== '') {
              const res = await fileService.getFile({ guid });
              const blob = new Blob([res.data], { type: 'audio/webm' });
              bolbURLList[index] = URL.createObjectURL(blob);
            }
          }
          vm.$set(vm.inputTail, 'blobURL', bolbURLList);
        }
        break;
      }
      default: {
        throw new Error(`not defined question type ${q.type}`);
      }
    }

    vm.$nextTick(() => {
      if (vm.inputBody.questionType === 'CONNECTION') {
        vm.$refs.questionEditor.handleChangeConnection();
      }
      vm.$set(vm.inputTail, 'questionFile', vm.$refs.questionEditor.$refs.questionUpload.uploadFiles);
      if (q.question_material.length !== 0) {
        for (let i = 0; i < q.question_material.length; i += 1) {
          vm.$refs.questionEditor.$refs.questionUpload.uploadFiles.push({
            name: q.question_material[i].name,
            file_guid: q.question_material[i].file_guid,
          });
        }
        // 문항 유형 변경시에도 기존 파일 목록 유지하기 위해 inputBody에 삽입.
        vm.$set(vm.inputBody, 'oldQuestionFile', q.question_material);
      }
    });
  }
  /* eslint-enable no-param-reassign */

  static async postChildLectureItem({ lcItemId, inputBody, inputTail }) {
    const res1 = await questionService.postQuestion({
      lectureItemId: lcItemId,
    });
    const questionId = res1.data.question_id;
    this.putChildLectureItem({ questionId, inputBody, inputTail });
  }

  static async putChildLectureItem({ questionId, inputBody, inputTail }) {
    const questionType = utils.convertQuestionType2(inputBody.questionType);

    await questionService.putQuestionType({
      questionId,
      type: questionType,
    });

    const answer = Array.isArray(inputTail.answer) ?
      inputTail.answer : [inputTail.answer];

    await questionService.putQuestion({
      questionId,
      question: inputTail.question,
      choice: inputTail.questionList,
      answer,
      choice2: inputTail.choice2,
      difficulty: inputTail.difficulty,
      language: inputTail.language,
      sampleCode: inputTail.code,
      inputDescription: inputTail.inputDescription,
      outputDescription: inputTail.outputDescription,
      sampleInput: inputTail.sampleInput,
      sampleOutput: inputTail.sampleOutput,
      timeLimit: inputTail.timeLimit,
      memoryLimit: inputTail.memoryLimit,
    });

    // 문제에 음성이 추가된 경우, 파일 업로드 절차
    if (inputTail.questionMediaType === 'voice') {
      // 새로 등록된 경우에만
      if (inputTail.questionVoiceFile !== undefined) {
        const res = await questionService.postQuestionQuestionVoiceFile({
          questionId,
          file: inputTail.questionVoiceFile,
        });
        const guid = res.data.file.file_guid;

        await questionService.putQuestion({
          questionId,
          questionMediaType: inputTail.questionMediaType,
          questionMedia: guid,
        });
      }
    }
    // 문제에 영상이 추가된 경우
    if (inputTail.questionMediaType === 'youtube') {
      await questionService.putQuestion({
        questionId,
        questionMediaType: inputTail.questionMediaType,
        questionMedia: inputTail.questionYoutubeURL,
      });
    }

    // 기존 파일 목록이 존재
    if (inputBody.oldQuestionFile !== undefined) {
      // 기존 파일 중 수정 파일 목록에 존재하지 않는 것을 삭제
      const deleteList = [];
      for (let i = 0; i < inputBody.oldQuestionFile.length; i += 1) {
        let existance = false;
        for (let j = 0; j < inputTail.questionFile.length; j += 1) {
          if (inputBody.oldQuestionFile[i].name === inputTail.questionFile[j].name) {
            existance = true;
            break;
          }
        }
        if (!existance) {
          deleteList.push({ file_guid: inputBody.oldQuestionFile[i].file_guid });
        }
      }
      for (let i = 0; i < deleteList.length; i += 1) {
        fileService.deleteFile({
          fileGuid: deleteList[i].file_guid,
        });
      }

      // 수정 파일 중 기존 파일 목록에 존재하지 않는 것을 추가
      const newList = [];
      for (let i = 0; i < inputTail.questionFile.length; i += 1) {
        let existance = false;
        for (let j = 0; j < inputBody.oldQuestionFile.length; j += 1) {
          if (inputTail.questionFile[i].name === inputBody.oldQuestionFile[j].name) {
            existance = true;
            break;
          }
        }
        if (!existance) {
          newList.push({ file: inputTail.questionFile[i].raw });
        }
      }
      for (let i = 0; i < newList.length; i += 1) {
        questionService.postQuestionFile({
          questionId,
          file: newList[i].file,
        });
      }
    }

    // 기존 파일 목록이 존재하지 않음
    if (inputBody.oldQuestionFile === undefined) {
      // 추가할 파일이 존재할 경우 파일 추가
      if (inputTail.questionFile.length !== 0) {
        for (let i = 0; i < inputTail.questionFile.length; i += 1) {
          questionService.postQuestionFile({
            questionId,
            file: inputTail.questionFile[i].raw,
          });
        }
      }
    }

    await questionService.deleteQuestionKeywords({
      questionId,
    });
    await questionService.postQuestionKeywords({
      questionId,
      data: inputTail.assignedKeywordList,
    });

    // 서술형인 경우
    if (inputBody.questionType === 'DESCRIPTION') {
      // 기존 파일 목록이 존재
      if (inputBody.oldAnswerFile !== undefined) {
        // 기존 파일 중 수정 파일 목록에 존재하지 않는 것을 삭제
        const deleteList = [];
        for (let i = 0; i < inputBody.oldAnswerFile.length; i += 1) {
          let existance = false;
          for (let j = 0; j < inputTail.answerFile.length; j += 1) {
            if (inputBody.oldAnswerFile[i].name === inputTail.answerFile[j].name) {
              existance = true;
              break;
            }
          }
          if (!existance) {
            deleteList.push({ file_guid: inputBody.oldAnswerFile[i].file_guid });
          }
        }

        for (let i = 0; i < deleteList.length; i += 1) {
          fileService.deleteFile({
            fileGuid: deleteList[i].file_guid,
          });
        }

        // 수정 파일 중 기존 파일 목록에 존재하지 않는 것을 추가
        const newList = [];
        for (let i = 0; i < inputTail.answerFile.length; i += 1) {
          let existance = false;
          for (let j = 0; j < inputBody.oldAnswerFile.length; j += 1) {
            if (inputTail.answerFile[i].name === inputBody.oldAnswerFile[j].name) {
              existance = true;
              break;
            }
          }
          if (!existance) {
            newList.push({ file: inputTail.answerFile[i].raw });
          }
        }
        for (let i = 0; i < newList.length; i += 1) {
          questionService.postQuestionAnswerFile({
            questionId,
            file: newList[i].file,
          });
        }
      }

      // 기존 파일 목록이 존재하지 않음
      if (inputBody.oldAnswerFile === undefined) {
        // 추가할 파일이 존재할 경우 파일 추가
        if (inputTail.answerFile.length !== 0) {
          for (let i = 0; i < inputTail.answerFile.length; i += 1) {
            questionService.postQuestionAnswerFile({
              questionId,
              file: inputTail.answerFile[i].raw,
            });
          }
        }
      }
    } else if (inputBody.oldAnswerFile !== undefined) {
      for (let i = 0; i < inputBody.oldAnswerFile.length; i += 1) {
        fileService.deleteFile({
          fileGuid: inputBody.oldAnswerFile[i].file_guid,
        });
      }
    }

    // 객관식인 경우
    if (inputBody.questionType === 'MULTIPLE_CHOICE') {
      switch (inputTail.multiChoiceMediaType) {
        case 'none': {
          await questionService.putQuestion({
            questionId,
            multiChoiceMediaType: inputTail.multiChoiceMediaType,
          });
          break;
        }
        case 'youtube': {
          // 주소를 배열로 묶어서 서버에 보낸다. 타입 = youtube도 함께 보낸다.
          await questionService.putQuestion({
            questionId,
            multiChoiceMediaType: inputTail.multiChoiceMediaType,
          });
          break;
        }
        case 'image': {
          // 여기서는 vm 객체에 접근할 수 없어서 LectureItemEditor 의 onSubmit 마지막 부분에서 전처리함
          const clientPathList = inputTail.questionList;
          for (const [index, file] of inputTail.multiChoiceMedia.entries()) {
          // 객관식 각 이미지를 파일로써 서버에 올리고, id를 받는다.
            if (file !== undefined && file.uid !== undefined) { // 새로 올린 파일이 있을때만 (수정시는 제외)
              const res = await questionService.postQuestionMultiChoiceFile({
                questionId,
                file,
              });
              clientPathList[index] = res.data.file.client_path;
            }
          }
          // id를 순서대로 배열로 묶어서 서버에 보낸다. 타입 = image도 함께 보낸다.
          await questionService.putQuestion({
            questionId,
            multiChoiceMediaType: inputTail.multiChoiceMediaType,
            choice: clientPathList,
          });
          break;
        }
        case 'voice': {
          // 음성을 각각 파일로 만들어서 서버에 올리고, id를 받는다.
          const guidList = inputTail.questionList;
          for (const [index, file] of inputTail.multiChoiceMedia.entries()) {
          // 객관식 각 이미지를 파일로써 서버에 올리고, id를 받는다.
            if (file !== undefined && file.type !== undefined) { // 파일 업로드 (빈 슬롯 제외)
              const res = await questionService.postQuestionMultiChoiceFile({
                questionId,
                file,
              });
              guidList[index] = res.data.file.file_guid;
            }
          }
          // id를 순서대로 배열로 묶어서 서버에 보낸다. 타입 = voice도 함께 보낸다.
          await questionService.putQuestion({
            questionId,
            multiChoiceMediaType: inputTail.multiChoiceMediaType,
            choice: guidList,
          });
          break;
        }
        default: {
          break;
        }
      }
    }

    if (inputBody.questionType === 'CONNECTION') {
      switch (inputTail.multiChoiceMediaType) {
        case 'none': {
          await questionService.putQuestion({
            questionId,
            multiChoiceMediaType: inputTail.multiChoiceMediaType,
          });
          break;
        }
        case 'youtube': {
          // 주소를 배열로 묶어서 서버에 보낸다. 타입 = youtube도 함께 보낸다.
          await questionService.putQuestion({
            questionId,
            multiChoiceMediaType: inputTail.multiChoiceMediaType,
          });
          break;
        }
        case 'image': {
          // 여기서는 vm 객체에 접근할 수 없어서 LectureItemEditor 의 onSubmit 마지막 부분에서 전처리함
          const clientPathList = inputTail.questionList;
          for (const [index, file] of inputTail.multiChoiceMedia.entries()) {
          // 객관식 각 이미지를 파일로써 서버에 올리고, id를 받는다.
            if (file !== undefined && file.uid !== undefined) { // 새로 올린 파일이 있을때만 (수정시는 제외)
              const res = await questionService.postQuestionMultiChoiceFile({
                questionId,
                file,
              });
              clientPathList[index] = res.data.file.client_path;
            }
          }
          // id를 순서대로 배열로 묶어서 서버에 보낸다. 타입 = image도 함께 보낸다.
          await questionService.putQuestion({
            questionId,
            multiChoiceMediaType: inputTail.multiChoiceMediaType,
            choice: clientPathList,
          });
          break;
        }
        case 'voice': {
          // 음성을 각각 파일로 만들어서 서버에 올리고, id를 받는다.
          const guidList = inputTail.questionList;
          for (const [index, file] of inputTail.multiChoiceMedia.entries()) {
          // 객관식 각 이미지를 파일로써 서버에 올리고, id를 받는다.
            if (file !== undefined && file.type !== undefined) { // 파일 업로드 (빈 슬롯 제외)
              const res = await questionService.postQuestionMultiChoiceFile({
                questionId,
                file,
              });
              guidList[index] = res.data.file.file_guid;
            }
          }
          // id를 순서대로 배열로 묶어서 서버에 보낸다. 타입 = voice도 함께 보낸다.
          await questionService.putQuestion({
            questionId,
            multiChoiceMediaType: inputTail.multiChoiceMediaType,
            choice: guidList,
          });
          break;
        }
        default: {
          break;
        }
      }
    }

    // SW인 경우
    // if (inputBody.questionType === 'SW') {
    //   // 기존 testCase가 있다면 싹다 지운다.
    //   if (inputTail.testCaseListOld !== undefined) {
    //     inputTail.testCaseListOld.forEach((element) => {
    //       questionService.deleteQuestionTestCase({
    //         questionId,
    //         num: element.num,
    //       });
    //     });
    //   }
    //   // 새로운 testCase 싹다 넣는다.
    //   await questionService.postQuestionTestCase({
    //     questionId,
    //     testcase: inputTail.testCaseList.map(testCase => ({
    //       ...testCase,
    //       memory: inputTail.memoryLimit,
    //       timeout: inputTail.timeLimit,
    //     })),
    //   });
    // }

    if (inputBody.questionType === 'SW') {
      // 새로운 testCase 싹다 넣는다.
      await questionService.postQuestionTestCase({
        questionId,
        testcase: inputTail.testCaseList.map(testcase => ({
          input: testcase.input,
          output: testcase.output,
          score_ratio: testcase.score_ratio,
          memory: inputTail.memoryLimit,
          timeout: inputTail.timeLimit,
        })),
      });
    }

    // SQL인 경우
    if (inputBody.questionType === 'SQL') {
      // 기존 파일 있었다.
      if (inputBody.sqlFileOld !== undefined) {
        // 현재 파일 없다.
        if (inputTail.sqlFile[0] === undefined) {
          // 기존파일 삭제
          fileService.deleteFileSqlite({
            fileGuid: inputBody.sqlFileOld.file_guid,
            questionId,
          });
        }
        // 현재 파일 새로올렸다.
        if (inputTail.sqlFile !== undefined
          && inputTail.sqlFile[0] !== undefined
          && inputTail.sqlFile[0].raw !== undefined) {
          // 기존파일 삭제
          await fileService.deleteFileSqlite({
            fileGuid: inputBody.sqlFileOld.file_guid,
            questionId,
          });
          // 현재 파일 등록
          questionService.postQuestionSQLiteFile({
            questionId,
            file: inputTail.sqlFile[0].raw,
          });
        }
        // 현재 파일 그대로다.
          // 할일 없음
      }
      // 기존 파일 없었다.
      if (inputBody.sqlFileOld === undefined) {
        // 현재 파일 새로올렸다.
        if (inputTail.sqlFile !== undefined
          && inputTail.sqlFile[0] !== undefined
          && inputTail.sqlFile[0].raw !== undefined) {
          // 현재파일 등록
          questionService.postQuestionSQLiteFile({
            questionId,
            file: inputTail.sqlFile[0].raw,
          });
        }
        // 현재 파일 없다.
          // 할일 없음
      }
    } else if (inputBody.sqlFileOld !== undefined) {
      fileService.deleteFile({
        fileGuid: inputBody.sqlFileOld.file_guid,
        questionId,
      });
    }

    // 멀티미디어형인 경우
    if (inputBody.questionType === 'MULTIMEDIA') {
      await questionService.putQuestion({
        questionId,
        answerMediaType: inputTail.answerMediaType,
      });
    }
  }
}
