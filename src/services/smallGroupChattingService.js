import http from './http';

export default {
  getChatting({
    room_id,
  }) {
    return http.get(`/small_group_chattings/${room_id}`);
  },
  deleteChatting({
    room_id,
  }) {
    return http.delete(`/small_group_chattings/${room_id}`);
  },
  postFile({
    room_id,
    file,
  }) {
    const form = new FormData();
    form.append('file', file, file.name);
    return http.post(`/small_group_chattings/file/${room_id}`, form, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
  getFiles({
    room_id,
  }) {
    return http.get(`/small_group_chattings/filelist/${room_id}`);
  },
  deleteFile({
    guid,
  }) {
    return http.delete(`/small_group_chattings/file/${guid}`);
  },
};
