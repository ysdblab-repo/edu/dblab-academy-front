import http from './http';

export default {
  getLectureList({ classId }) {
    return http.get(`/reports/${classId}`);
  },
  getLectureItems({ lectureId }) {
    return http.get(`/reports/lectures/${lectureId}`);
  },
  getClassResult({ classId, type }) {
    return http.get(`/reports/class-result/${classId}/type/${type}`);
  },
  getLectureCheck({ lectureId }) {
    return http.get(`/reports/check-lecture/${lectureId}`);
  },
  postLectureResult({ lectureId, result, type }) {
    return http.post(`reports/lectures/${lectureId}`, {
      result,
      type,
    });
  },
};
