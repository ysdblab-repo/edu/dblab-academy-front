import http from './http';

export default {
  postDiscussion({
    lecture_item_id,
    content,
  }) {
    return http.post('/discussions', {
      lecture_item_id,
      content,
    });
  },
  putDiscussion({
    lecture_item_id,
    content,
  }) {
    return http.put(`/discussions/${lecture_item_id}`, { // eslint-disable-line
      content,
    });
  },
  // putDiscussion({
  //               id,
  //               share,
  //               topic,
  //             }) {
  //   return http.put(`/discussions/${id}`, {
  //     share,
  //     topic,
  //   });
  // },
  getDiscussion({
    id,
  }) {
    return http.get(`/discussions/${id}`);
  },
  getDiscussionKeywords({
    discussionId,
  }) {
    return http.get(`/discussions/${discussionId}/keywords`);
  },
  postdiscussionKeywords({
    discussionId,
    data,
  }) {
    return http.post(`/discussions/${discussionId}/keywords`, {
      data,
    });
  },
  deleteDiscussionKeywords({
    discussionId,
  }) {
    return http.delete(`/discussions/${discussionId}/keywords`);
  },
};
