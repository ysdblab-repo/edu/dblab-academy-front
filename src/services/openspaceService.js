import http from './http';

export default {
  get1stCategories() {
    return http.get('/open_space');
  },
  get2ndCategoriesBy1st({
    id
  }) {
    return http.get(`/open_space/${id}`);
  },
  get2ndCategories() {
    return http.get(`/open_space/second`);
  },
  get2ndCategoriesById({
    id_1st,
    name
  }) {
    return http.get(`/open_space/second/list?id_1st=${id_1st}&name=${name}`);
  },
  getSearchAffiliation({
    name
  }) {
    return http.get(`/open_space/find/${name}`);
  },
  postOpenInstitutionUserClass({
    classId,
  }) {
    return http.post(`/classes/binding`, {
      classId,
    });
  },
  getInstitutionList({
    affiliationId,
  }){
		return http.get(`/classes/institution/${affiliationId}`);
  },
  getPreswotNotifyList() {
		return http.get(`/open_space/preswotNotify`);
  },
  getOpenAffiliationById({
    id,
  }) {
    return http.get(`/classes/open/affiliation/${id}`);
  },
  // 2021-05-11 구독중인 기관 ID 리스트 조회
  getOpenAffiliationFollowList({
    id,
  }) {
    return http.get(`/classes/open/affiliation/follow/${id}`);
  },
  // 2021-05-10 기관 Follow
  postAffiliationFollow({
    affiliationId,
    userId,
  }){
    return http.post(`/classes/open/affiliation/follow`, {
      affiliationId,
      userId,
    });
  },
  // 2021-05-10 기관 unFollow
  deleteAffiliationFollow({
    affiliationId,
    userId,
  }){
    return http.delete(`/classes/open/affiliation/follow/${userId}/${affiliationId}`);
  },
};
