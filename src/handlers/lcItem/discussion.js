import LcItemHandler from './index';
import discussionService from '../../services/discussionService';
// import utils from '../../utils';

export default class SurveyHandler extends LcItemHandler {
  static initViewModel(vm) {
    const item = vm.lectureItem;
    // console.log('item = ', item);
    // const d = item.discussion_info;

    // TODO : '토론' 이 필요하다면 추후에 개방
    /*
    const keywordList = await practiceService.getPracticeKeywords({
      practiceId: p.practice_id,
    });
    keywordList.data.forEach((element) => {
      element.score = element.score_portion;
    });
    vm.inputTail.assignedKeywordList = keywordList.data;
    */
    vm.$set(vm.inputTail, 'content', item.discussions[0].content);
  }

  // @Override
  static async postChildLectureItem({ lcItemId, inputTail }) { // eslint-disable-line no-unused-vars, max-len
    await discussionService.postDiscussion({
      lecture_item_id: lcItemId,
      content: inputTail.content,
    });
    // this.putChildLectureItem({ lectureItemId: lcItemId, inputTail });
  }
  static async putChildLectureItem({ lectureItemId, inputTail }) {
    await discussionService.putDiscussion({
      lecture_item_id: lectureItemId,
      content: inputTail.content,
    });
    // discussionService.putDiscussion({
    //   id: lectureItemId,
    //   share: true, // TODO share 값 입력 필요하다면 구현할 것
    //   topic: inputTail.content,
    // });

    /*
    await discussionService.deletePracticeKeywords({
      practiceId,
    });
    await practiceService.postPracticeKeywords({
      practiceId,
      data: inputTail.assignedKeywordList,
    });
    */
  }
}
