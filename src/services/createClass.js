import http from './http';

// service 관련 documentation은 구글 스프레드 시트를 참조하는게 빠름!
export default {
  requestClass({
    subjectType,
    affiliationId,
    class_type,
    className,
    class_detail,
    id_2nd,
  }) {
    return http.post('/req_create_class', {
      subject_type: subjectType,
      affiliation_id: affiliationId,
      type: class_type,
      class_name: className,
      comment: class_detail,
      class_id: null,
      id_2nd,
    });
  },
  searchaRequestStatus({
    userId,
  }) {
    return http.get(`/req_create_class/userId/${userId}`);
  },
  getUserAffiliation({
    userId,
  }) {
    return http.get(`/user_belonging?user_id=${userId}`);
  },
  searchAffId({
    userId,
  }) {
    return http.get('/createClass/search_affiliation', {
      params: {
        user_id: userId
      }
    });
  },
  postRequestClass({
    subjectType,
    affiliationId,
    class_type,
    className,
    class_detail,
  }) {
    return http.post('/req_create_class/admin', {
      subject_type: subjectType,
      affiliation_id: affiliationId,
      type: class_type,
      class_name: className,
      comment: class_detail,
      class_id: null,
    });
  },
  createClassPasswordByAdmin({
    password,
    req_id,
  }) {
    return http.post('/req_create_class/class_passwords_by_admin', {
      password,
      req_id,
    });
  },
  getClassPasswordByPassword({ pwd }) {
    return http.get(`/req_create_class/class/password/${pwd}`);
  },
  getClassPassword() {
    return http.get(`/req_create_class/class_password`);
  },
  getReqList({ id }) {
    return http.get(`/req_create_class/${id}`);
  },
  putReqList({ id }) {
    return http.put(`/req_create_class/${id}`);
  },
  deleteClassPassword({ // 게시글 삭제
    id,
  }) {
    return http.delete(`/req_create_class/${id}`);
  },
  getOpenSpaceAffiliation() {
    return http.get(`/user_belonging/openSpaceList`);
  },
};
