import http from './http';

export default {
  postCoding({
    lectureItemId,
  }) {
    return http.post('/coding_assignment', {
      lectureItemId,
    });
  },
  putCoding({
    codingId,
    description,
  }) {
    return http.put(`/coding_assignment/${codingId}`, {
      description,
    });
  },
  getCodingKeywords({
    codingId,
  }) {
    return http.get(`/coding_assignment/${codingId}/keywords`);
  },
  postCodingKeywords({
    codingId,
    data,
  }) {
    return http.post(`/coding_assignment/${codingId}/keywords`, {
      data,
    });
  },
  deleteCodingKeywords({
    codingId,
  }) {
    return http.delete(`/coding_assignment/${codingId}/keywords`);
  },
  getCodingResults({
    classId,
  }) {
    return http.get(`/coding_assignment/${classId}/results`);
  },
  postUpload({
    classId,
    itemId
  }) {
    return http.post(`/coding_assignment/upload`, {
      classId,
      itemId
    });
  }
};
