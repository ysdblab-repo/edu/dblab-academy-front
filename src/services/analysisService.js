import http from './http';


/**
 * service 관련 documentation은 구글 스프레드 시트를 참조하는게 빠름!
 */
export default {
  NNgetUnderstanding({
    classId,
  }) {
    return http.get(`/journalings2/understanding/${classId}`);
  },
  NNgetParticipation({
    classId,
  }) {
    return http.get(`/journalings2/participation/${classId}`);
  },
  getClassLogAnalysis({
                         userId, classId, isStudent,
                       }) {
    return http.get(`/journalings/student_class_logs/${userId}/${classId}/${isStudent}`);
  },
  getLectureLogAnalysis({
                   userId, lectureId, isStudent,
                 }) {
    return http.get(`/journalings/${userId}/${lectureId}/${isStudent}`);
  },
  getClassKeyword({
                          userId, classId, isStudent,
                        }) {
    return http.get(`/journalings/student_class_keyword/${userId}/${classId}/${isStudent}`);
  },
  getLectureKeyword({
                      userId, lectureId, isStudent,
                    }) {
    return http.get(`/journalings/student_lecture_keyword/${userId}/${lectureId}/${isStudent}`);
  },
  getParticipation({
    lectureId,
  }) {
    return http.get(`/journalings2/participation/${lectureId}`);
  },
  getUnderstanding({
    lectureId,
  }) {
    return http.get(`/journalings2/understanding/${lectureId}`);
  },
  getConcentration({
    lectureId,
  }) {
    return http.get(`/journalings2/concentration/${lectureId}`);
  },
  getKeywordItems({
    lectureId,
  }) {
    return http.get(`/journalings2/keyword-items/${lectureId}`);
  },
  getLectureStudents({
    classId,
  }) {
    return http.get(`/journalings2/students/${classId}`);
  },
  getJournaling({
    classId,
    lectureId,
    itemId,
    view,
  }) {
    return http.get('/journalings3', {
      params: {
        classId,
        lectureId,
        itemId,
        view,
      },
    });
  },
  /*
  getBaseInfo({
    classId,
    lectureId,
    itemId,
  }) {
    return http.get('/journalings3/baseInfo', {
      params: {
        classId,
        lectureId,
        itemId,
      },
    });
  },
  */
  getQuestionItem({
    lectureItemId,
    lectureId,
    classId,
  }) {
    return http.get('/journaling4/question_item', {
      params: {
        lecture_item_id: lectureItemId,
        lecture_id: lectureId,
        class_id: classId,
      },
    });
  },
  getBaseInfo({
    studentId,
    lectureItemId,
    lectureId,
    classId,
  }) {
    return http.get('/journaling4/baseInfo', {
      params: {
        student_id: studentId,
        lecture_item_id: lectureItemId,
        lecture_id: lectureId,
        class_id: classId,
      },
    });
  },
  getQuestionLecture({
    classId,
  }) {
    return http.get('/journaling4/question_lecture', {
      params: {
        class_id: classId,
      },
    });
  },
  getQuestionItemType({
    lectureItemId,
    lectureId,
    classId,
  }) {
    return http.get('/journaling4/question_item/type', {
      params: {
        lecture_item_id: lectureItemId,
        lecture_id: lectureId,
        class_id: classId,
      },
    });
  },
  getQuestionLectureType({
    classId,
  }) {
    return http.get('/journaling4/question_lecture/type', {
      params: {
        class_id: classId,
      },
    });
  },
  getQuestionKeyword({
    classId,
    lectureId
  }){
    return http.get('/journaling4/keyword_journaling',{
      params: {
        class_id: classId,
        lecture_id: lectureId,
      },
    });
  },
  getQuestionKeywordClass({
    classId,
  }){
    return http.get('/journaling4/keyword_journaling/class',{
      params: {
        class_id: classId,
      },
    });
  },
};
