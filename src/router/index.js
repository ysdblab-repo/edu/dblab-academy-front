import Vue from 'vue';
import Router from 'vue-router';
import Login from '../components/pages/Login';
import FindingPasswordPage from '../components/pages/FindingPasswordPage';
import Tos from '../components/pages/Tos';
import NNTos from '../components/pages/NNTos';
import Register from '../components/pages/v4_register';
import DownloadPage from '../components/pages/DownloadPage';
import PolicyPage from '../components/pages/PolicyPage';
import NotFound from '../components/pages/NotFound';
import ClassIndex from '../components/pages/ClassIndex';
import StudentLectureMultiLive from '../components/pages/StudentLectureMultiLive';
// import ClassIndexCard from '../components/pages/ClassIndexCard';
import ClassDetail from '../components/pages/ClassDetail';
// import Test from '../components/TestComponent';
import authRoutes from './authRoutes';
import test2Assignment from '../components/pages/test2Assignment'
import test2Board from '../components/pages/test2Board'
import test2Notice from '../components/pages/test2Notice'
import v4_login from '../components/pages/v4_login';
import test2NNClassQuestionAnswerDetail from '../components/pages/test2NNClassQuestionAnswerDetail'
import test2NNClassQestionAnswerWrite from '../components/pages/test2NNClassQestionAnswerWrite'
import test2NNClassNoticeDetail from '../components/pages/test2NNClassNoticeDetail';
import store from '../stores';

Vue.use(Router);

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '/v4/test2/test2Assignment',
      name: 'v4_test2/test2Assignment',
      component: test2Assignment,
    },
    {
      path: '/',
      name: '/',
      redirect: '/a'
    },
    {
      path: '/v4/test2/:classId/test2Board/:className',
      name: 'v4_test2/test2Board',
      component: test2Board,
    },
    {
      path: '/v4/test2/:classId/test2Notice/:className',
      name: 'v4_test2/test2Notice',
      component: test2Notice,
    },
    {
      path: '/v4/test2/:classId/test2NNClassQuestionAnswerDetail/:boardId',
      name: 'v4_test2/test2NNClassQuestionAnswerDetail',
      component: test2NNClassQuestionAnswerDetail,
    },
    {
      path: '/v4/test2/:classId/test2NNClassQestionAnswerWrite',
      name: 'v4_test2/test2NNClassQestionAnswerWrite',
      component: test2NNClassQestionAnswerWrite,
      meta: {
        title: 'Class Q&A Write',
      },
    },
    {
      path: '/v4/test2/:classId/test2NNClassNoticeDetail/:noticeId',
      name: 'v4_test2/test2NNClassNoticeDetail',
      component: test2NNClassNoticeDetail,
      meta: {
        title: 'Class Notice Detail',
      },
    },
    
    {
      path:'/v4/StudentLectureMultiLive/:lectureId/:classId',
      name:'v4_StudentLectureMultiLive',
      component:StudentLectureMultiLive,
    },
    {
      path: '/login',
      name: 'Login',
      component: v4_login,
      beforeEnter: (to, from , next) => {
        store.state.auth.jwt = '';
        next();
      }
    },
    {
      path: '/forgot-password/:rand/:userId',
      name: 'FindingPasswordPage',
      component: FindingPasswordPage,
    },
    {
      path: '/PPtos',
      name: 'Tos',
      component: Tos,
    },
    {
      path: '/tos',
      name: 'NNTos',
      component: NNTos,
    },
    {
      path: '/register',
      name: 'Register',
      component: Register,
      beforeEnter: (to, from , next) => {
        store.state.auth.jwt = '';
        next();
      }
    },
    {
      path: '/download',
      name: 'Download',
      component: DownloadPage,
    },
    {
      path: '/policy',
      name: 'Policy',
      component: PolicyPage,
    },
    {
      path: '/classes',
      name: 'ClassIndex',
      component: ClassIndex,
    },
    {
      path: '/class/:classId/classdetail',
      name: 'ClassDetail',
      component: ClassDetail,
      meta: {
        title: 'Class Detail',
        auth: true,
      },
    },
    ...authRoutes('/a'),
    {
      path: '*',
      name: 'NotFound',
      component: NotFound,
    },
  ],
});