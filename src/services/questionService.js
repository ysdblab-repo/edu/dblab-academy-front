import http from './http';
import utils from '../utils';

export default {
  postQuestion({
    lectureItemId,
  }) {
    return http.post('/questions', {
      lectureItemId,
    });
  },
  postTestcaseRun(language, code, input) {
    return http.post('/questions/testcase/output', {
      language,
      code,
      input
    });
  },
  putQuestion({
    questionId,
    question,
    choice,
    answer,
    choice2,
    isOrderingAnswer,
    score,
    difficulty,
    timer,
    inputDescription,
    outputDescription,
    sampleCode,
    sampleInput,
    sampleOutput,
    timeLimit,
    memoryLimit,
    language,
    multiChoiceMediaType,
    multiChoiceMedia,
    answerMediaType,
    questionMediaType,
    questionMedia,
  }) {
    const param = {};

    utils.assignIfNotNil(param, { question });
    utils.assignIfNotNil(param, { choice });
    utils.assignIfNotNil(param, { answer });
    utils.assignIfNotNil(param, { isOrderingAnswer });
    utils.assignIfNotNil(param, { score });
    utils.assignIfNotNil(param, { difficulty });
    utils.assignIfNotNil(param, { timer });
    utils.assignIfNotNil(param, { inputDescription }, 'input');
    utils.assignIfNotNil(param, { outputDescription }, 'output');
    utils.assignIfNotNil(param, { sampleInput });
    utils.assignIfNotNil(param, { sampleOutput });
    utils.assignIfNotNil(param, { timeLimit });
    utils.assignIfNotNil(param, { memoryLimit });
    utils.assignIfNotNil(param, { language }, 'accept_language');
    utils.assignIfNotNil(param, { multiChoiceMediaType });
    utils.assignIfNotNil(param, { multiChoiceMedia });
    utils.assignIfNotNil(param, { answerMediaType });
    utils.assignIfNotNil(param, { questionMediaType });
    utils.assignIfNotNil(param, { questionMedia });
    utils.assignIfNotNil(param, { choice2 });
    utils.assignIfNotNil(param, { sampleCode }, 'sample_code');

    return http.put(`/questions/${questionId}`, param);
  },
  putQuestionType({
    questionId,
    type,
  }) {
    return http.put(`/questions/${questionId}/type`, {
      type,
    });
  },
  postQuestionTestCase({
    questionId,
    testcase,
  }) {
    return http.post(`/questions/${questionId}/testcases`, {
      testcase,
    });
  },
  putQuestionTestCase({
    questionId,
    num,
    input,
    output,
  }) {
    return http.put(`/questions/${questionId}/testcases/${num}`, {
      input,
      output,
    });
  },
  deleteQuestionTestCase({
    questionId,
    num,
  }) {
    return http.delete(`/questions/${questionId}/testcases/${num}`);
  },
  postQuestionAnswerFile({
    questionId,
    file,
  }) {
    const form = new FormData();
    form.append('file', file, file.name);
    return http.post(`/questions/${questionId}/file`, form, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
  postQuestionFile({
    questionId,
    file,
  }) {
    const form = new FormData();
    form.append('file', file, file.name);
    return http.post(`/questions/${questionId}/material-file`, form, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
  postQuestionMultiChoiceFile({
    questionId,
    file,
  }) {
    const form = new FormData();
    form.append('file', file, file.name);
    return http.post(`/questions/${questionId}/multichoice-file`, form, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
  postQuestionQuestionVoiceFile({
    questionId,
    file,
  }) {
    const form = new FormData();
    form.append('file', file, file.name);
    return http.post(`/questions/${questionId}/question-voice-file`, form, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
  postQuestionSQLiteFile({
     questionId,
     file,
   }) {
    const form = new FormData();
    form.append('file', file, file.name);
    return http.post(`/questions/${questionId}/sql-lite-file`, form, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
  getQuestionResult({
    questionId,
  }) {
    return http.get(`/questions/${questionId}/result`);
  },
  NNgetQuestionResult({
    questionId,
    classId,
  }) {
    return http.get(`/questions/${questionId}/${classId}/result`);
  },
  selectPreviousSqlite({
    guid, questionId,
  }) {
    return http.put(`/questions/${questionId}/sqlite/${guid}`);
  },
  extractAnswer({
    lectureItemId,
    ratioStudent,
  }) {
    return http.post(`/questions/${lectureItemId}/extract-answer/`, {
      ratioStudent,
    });
  },
  autoGradeDescription({
    lectureItemId,
    teacherSideList,
    studentSideList,
  }) {
    return http.post(`/questions/${lectureItemId}/auto-grade-description`, {
      teacherSideList,
      studentSideList,
    });
  },

  getQuestionKeywords({
    questionId,
  }) {
    return http.get(`/questions/${questionId}/keywords`);
  },
  postQuestionKeywords({
    questionId,
    data,
  }) {
    return http.post(`/questions/${questionId}/keywords`, {
      data,
    });
  },
  deleteQuestionKeywords({
    questionId,
  }) {
    return http.delete(`/questions/${questionId}/keywords`);
  },
};
