import http from './http';


/**
 * service 관련 documentation은 구글 스프레드 시트를 참조하는게 빠름!
 */
export default {
  getStudentLectureLog({
               lectureId, opt,
             }) {
    return http.get(`/student_lecture_logs/get_by_lecture/${lectureId}/${opt}`);
  },
  getStatus({ // 현재 p2p 실시간 강의 진행중인지 여부
    room_id
  }) {
    return http.get(`/realtime_lecture_room?room_id=${room_id}`);
  },
};
