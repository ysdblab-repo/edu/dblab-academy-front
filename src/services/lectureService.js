import http from './http';
import utils from '../utils';

/**
 * service 관련 documentation은 구글 스프레드 시트를 참조하는게 빠름!
 */
export default {
  getStudentStatus({
    lectureId
  }) {
    // return http.get(`/lectures/${lectureId}/student/status`, {
    //   lectureId
    // });
    return http.face.get(`/face/${lectureId}/concentration`);
  },
  postLectureAudioFile({ // 개별 파일 업로드
    file,
  }) {
    const form = new FormData();
    form.append('file', file, file.name);
    return http.post('/files/', form, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
  faceRecognitionSending({
    lectureId,
    imgCount,
    faceCount,
  }) {
    return http.post('/lectures/faceRecognition', {
      lectureId,
      imgCount,
      faceCount,
    });
    // return http.face.post('/face/faceRecognition', {
    //   lectureId,
    //   imgCount,
    //   faceCount,
    // });
  },
  postGradeLecture({
    lectureId
  }) {
    return http.post(`/lectures/lecture_grade/${lectureId}`);
  },
  getLecture({
    lectureId,
  }) {
    return http.get(`/lectures/${lectureId}`);
  },
  postLecture({
    classId,
    type,
    name,
    start_time,
    end_time,
  }) {
    return http.post('/lectures', {
      classId,
      type,
      name,
      start_time,
      end_time,
    });
  },
  postLecturePlist({
    lectureId, movedKeys,
  }) {
    return http.post(`/lectures/${lectureId}/plist`, {
      movedKeys,
    });
  },
  getLecturePlist({
    lectureId,
  }) {
    return http.get(`/lectures/${lectureId}/plist`);
  },
  deleteLecture({
    lectureId,
  }) {
    return http.delete(`/lectures/${lectureId}`);
  },
  putLecture() {
    if (arguments.length !== 1) {
      throw `lectureService.putLecture argument.length must be 1, current: ${argument.length}`;
    }
    
    const { lecture_id, ...param } = arguments[0];

    for (const key of Object.keys(param)) {
      if (key !== utils.snakeCase(key)) {
        throw `lectureService.putLecture allow only snake case attributes`;
      }
    }

    if (!lecture_id) {
      throw `In lectureService.putLecture, lecture_id is undefined`;
    }

    return http.put(`/lectures/${lecture_id}`, param);
  },
  // putLecture({
  //   lectureId,
  //   type,
  //   name,
  //   description,
  //   location,
  //   startTime,
  //   endTime,
  //   opened,
  //   mediaType,
  //   videoLink,
  //   teacherEmail,
  //   keywordState,
  //   sequence,
  //   audioGuid,
  // }) {
  //   const param = {};

  //   utils.assignIfNotNil(param, { lectureId });
  //   utils.assignIfNotNil(param, { type });
  //   utils.assignIfNotNil(param, { name });
  //   utils.assignIfNotNil(param, { description });
  //   utils.assignIfNotNil(param, { location });
  //   utils.assignIfNotNil(param, { startTime }, 'start_time');
  //   utils.assignIfNotNil(param, { endTime }, 'end_time');
  //   utils.assignIfNotNil(param, { opened });
  //   utils.assignIfNotNil(param, { mediaType }, 'media_type');
  //   utils.assignIfNotNil(param, { videoLink }, 'video_link');
  //   utils.assignIfNotNil(param, { audioGuid }, 'audio_guid');
  //   utils.assignIfNotNil(param, { teacherEmail }, 'teacher_email');
  //   utils.assignIfNotNil(param, { keywordState }, 'keyword_state');
  //   utils.assignIfNotNil(param, { sequence }); // 강의 순서 저장

  //   // console.log('putLecture param', param);
  //   return http.put(`/lectures/${lectureId}`, param);
  // },
  getLectureKeywords({
    lectureId,
  }) {
    return http.get(`/lectures/${lectureId}/keywords`);
  },
  postLectureKeywords({
    lectureId,
    lectureKeywords,
  }) {
    return http.post(`/lectures/${lectureId}/keywords`, {
      data: lectureKeywords,
    });
  },
  deleteLectureKeyword({
    lectureId,
    lectureKeyword,
  }) {
    return http.delete(`/lectures/${lectureId}/keywords/${lectureKeyword}`);
  },
  NNpostLectureKeyword({
    lectureId,
    keyword,
    weight,
  }) {
    return http.post(`/lectures/${lectureId}/keyword`, {
      keyword,
      weight,
    });
  },
  NNdeleteLectureKeyword({
    lectureId,
    keywordId,
  }) {
    return http.delete(`/lectures/${lectureId}/keyword/${keywordId}`);
  },
  NNputLectureKeyword({
    lectureId,
    keywordId,
    keyword,
    weight,
  }) {
    return http.put(`/lectures/${lectureId}/keyword/${keywordId}`, {
      keyword,
      weight,
    });
  },
  getLectureKeywordRelations({
    lectureId,
  }) {
    return http.get(`/lectures/${lectureId}/keyword-relations`);
  },
  postLectureKeywordRelations({
    lectureId,
    lectureRelations,
  }) {
    return http.post(`/lectures/${lectureId}/keyword-relations`, {
      data: lectureRelations,
    });
  },
  deleteLectureKeywordRelation({
    lectureId,
    node1,
    node2,
  }) {
    return http.delete(`/lectures/${lectureId}/keyword-relations/${node1}/${node2}`);
  },
  getLectureCoverage({ id }) {
    return http.get(`/lectures/${id}/coverage`);
  },
  executeExtractor({ id, numberOfKeyword, keywordLength }) {
    const param = {
      numberOfKeyword,
      minKeywordLength: keywordLength,
      isFast: true,
    };
    return http.post(`/lectures/${id}/keyword-extractor`, param);
  },
  postLectureMaterial({ lectureId, file }) {
    const form = new FormData();
    form.append('lecture_Id', lectureId);
    form.append('material_type', 0);
    form.append('file', file, file.name);
    return http.post('/materials', form, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  },
  getLectureMaterial({ lectureId }) {
    return http.get(`/materials/${lectureId}`);
  },
  getOpenedLectureItem({ lectureId }) {
    return http.get(`/lectures/${lectureId}/opened-item`);
  },
  getOpenedGroupItem({ lectureId }) {
    return http.get(`/lectures/group/${lectureId}`);
  },
  deleteMaterial({ id }) {
    return http.delete(`/materials/${id}`);
  },
  getMaterialKeywords({ id }) {
    return http.get(`/materials/${id}/keywords`);
  },
  getWholeStudents({ id }) {  // 전체 수강생 명단 불러오기
    return http.get(`/lectures/${id}/student-list`);
  },
  /* 삭제 - 181214
  getOnStudentCount({ lectureId }) {
    return http.get(`lectures/${lectureId}/on-student-count`);
  },
  */
  postMaterialKeyword({ id, keyword, score }) {
    return http.post(`/materials/${id}/keywords`, {
      keyword,
      score,
    });
  },
  deleteMaterialKeyword({ id, keyword }) {
    return http.delete(`/materials/${id}/keywords/${keyword}`);
  },
  getAllFilesUnderLecture({
    lectureId,
  }) {
    return http.get(`/lectures/${lectureId}/files`);
  },
  autoExtractKeyword({
    lectureId,
    guidList,
    w1,
    w2,
  }) {
    return http.post(`/lectures/${lectureId}/extract-keywords`, {
      guidList,
      w1,
      w2,
    });
  },
  postMainVideoKeyword({
    lectureId,
    type,
    data,
  }) {
    return http.post(`/lectures/${lectureId}/material-keyword`, {
      type,
      data,
    });
  },
  getMainVideoKeyword({
    lectureId,
  }) {
    return http.get(`/lectures/${lectureId}/material-keyword`);
  },
  deleteMainVideoKeyword({
    lectureId,
  }) {
    return http.delete(`/lectures/${lectureId}/material-keyword`);
  },
  studentLoginLog({
    lectureId,
  }) {
    return http.get(`/lectures/${lectureId}/is-lectured`);
  },
  changeResultOpen({
    lectureId,
    result,
  }) {
    return http.get(`/lectures/${lectureId}/result-opened/${result}`);
  },
  // 테스트 모드 변경 190805
  changeTestMode({
    lectureId,
    test,
  }) {
    return http.get(`/lectures/${lectureId}/test-mode/${test}`);
  },
  // hashTable 받아오기 190806
  getHashTable({
    hashId,
    len,
  }) {
    return http.get(`/lectures/getHash/${hashId}/${len}`);
  },
  // 학생 문제 출제 관리 API들
  registerStudentSubmit({
    lectureId,
    homeworkStartAt,
    homeworkEndAt,
    scoringStartAt,
    scoringEndAt,
    evalSelect,
    evaluatedCnt,
  }) {
    return http.post(`/lectures/${lectureId}/homework`, {
      homeworkStartAt,
      homeworkEndAt,
      scoringStartAt,
      scoringEndAt,
      evalSelect,
      evaluatedCnt,
    });
  },
  // 2020.06.05 MAX score 추가
  registerStudentSubmit2({
    lectureId,
    homeworkStartAt,
    homeworkEndAt,
    scoringStartAt,
    scoringEndAt,
    evalSelect,
    evaluatedCnt,
    maxScore,
    comment,
  }) {
    return http.post(`/lectures/${lectureId}/homework2`, {
      homeworkStartAt,
      homeworkEndAt,
      scoringStartAt,
      scoringEndAt,
      evalSelect,
      evaluatedCnt,
      maxScore,
      comment
    });
  },
  deleteStudentSubmit({
    lectureId,
  }) {
    return http.delete(`/lectures/${lectureId}/homework`);
  },
  // 2020.06.05 과제 정보 가져오기
  getLectureHomework({
    lectureId,
  }) {
    return http.get(`lectures/${lectureId}/getHomework`);
  },
  loadStudentSubmit({
    lectureId,
  }) {
    return http.get(`/lectures/${lectureId}/homework`);
  },
  loadStudentSubmitResult({
    lectureId,
  }) {
    return http.get(`/lectures/${lectureId}/homework/result`);
  },
  notSubmitHomework({
    lectureId,
  }) {
    return http.get(`/lectures/${lectureId}/homework-attendance`);
  },
  notScoringHomework({
    lectureId,
  }) {
    return http.get(`/lectures/${lectureId}/scoring-attendance`);
  },
  // 2020.05.21
  getThisStudentEstimated({ // 이 학생이 평가한 문제
    lectureId, studentId,
  }) {
    return http.get(`/lectures/${lectureId}/homework2/${studentId}`);
  },
  // 2020.05.22
  getEstimatedQuestion({ // 평가한 문제 가져오기
    questionId,
  }) {
    return http.get(`/lectures/${questionId}/homework3`);
  },
  studentQuestion({
    lectureId,
    questionId,
  }) {
    return http.get(`/lectures/${lectureId}/homework/${questionId}`);
  },
  studentQuestionFileList({
    lectureId, questionId,
  }) {
    return http.get(`/student/${lectureId}/question/${questionId}/file`);
  },
  // 학생이 접속했을 때 출석인증 중인지
  ifAuthenticationStart({
    lectureId,
  }) {
    return http.get(`/lectures/${lectureId}/authentication`);
  },
  // lectureID에 할당된 회의방 ID를 가져온다.
  getSmallRoomId({
    lectureId,
  }) {
    return http.get(`/lectures/smallroomid/${lectureId}`);    
  },
  // 소그룹방 생성
  postSmallMeetingRoom({
    classId,
    type,
    name,
    max_occupancy,
    password,
    lectureId,
  }) {
    if( password === '' )password = null;
    const param = {};
    utils.assignIfNotNil(param, { classId });
    utils.assignIfNotNil(param, { type });
    utils.assignIfNotNil(param, { name });
    utils.assignIfNotNil(param, { max_occupancy });
    utils.assignIfNotNil(param, { password });  
    utils.assignIfNotNil(param, { lectureId });  
    
    return http.post('/small_meeting_room', param);
  },
  // 소그룹방 수정
  putSmallMeetingRoom({
    roomId,
    name,
    max_occupancy,
    password,
  }) {
    if( password === '' )password = null;
    const param = {};
    utils.assignIfNotNil(param, { roomId });
    utils.assignIfNotNil(param, { name });
    utils.assignIfNotNil(param, { max_occupancy });
    utils.assignIfNotNil(param, { password });
    return http.put(`/small_meeting_room/${roomId}`, {
      name,
      max_occupancy,
      password,
    });
  },
  // 소그룹방 목록 불러오기
  getSmallMeetingRoomList({
    classId,
  }) {
    return http.get(`/small_meeting_room/${classId}`);
  },
  // 소그룹방 목록 불러오기
  newgetSmallMeetingRoomList({
    classId,
  }) {
    return http.get(`/small_meeting_room/${classId}`);
  },
  // 소그룹방 삭제
  deleteSmallMeetingRoom({
    roomId,
  }) {
    return http.delete(`/small_meeting_room/${roomId}`);
  },
  getEntraceCheck({
    roomId,
  }) {
    return http.get(`/small_meeting_room/entrace_check/${roomId}`);
  },
  getPasswordCheck({
    roomId,
    password,
  }) {
    return http.get(`/small_meeting_room/password_check/${roomId}`, {
      params: {
        password,
      },
    });
  },
  // 소그룹방 입장 권한 삭제
  deleteEntrancePermission({
    roomId,
  }) {
  return http.delete(`/small_meeting_room/permission/${roomId}`);
  },
  // 소그룹방 입장 권한 부여
  postEntrancePermission({
    roomId,
    userId,
  }) {
    return http.post(`/small_meeting_room/permission/${roomId}/${userId}`);
  },
  // 소그룹방 별 입장 권한 불러오기
  getPermittedUserList({
    roomId,
  }) {
    return http.get(`/small_meeting_room/permission/${roomId}`);
  },
  // 소그룹방 별 입장 권한 불러오기(class의 모든 권한)
  getPermittedUserListOfClass({
    classId,
  }) {
    return http.get(`/small_meeting_room/permission/class/${classId}`);
  },
  // 소그룹방 입장/퇴장 로그 생성
  postSGroupRoomLog({
    roomId,
  }) {
    return http.post(`/small_meeting_room/log/${roomId}`);
  },
  // 소그룹방 로그 삭제
  deleteSGroupRoomLog({
    roomId,
  }) {
    return http.delete(`/small_meeting_room/log/${roomId}`);
  },
  // 소그룹방 현재 인원 계산
  getCurrentStudentCount({
    roomId,
  }) {
    return http.get(`/small_meeting_room/log/${roomId}`);
  },
  postConnectSession({
    classId,
    source,
    target,
    conditionLower,
    conditionUpper,
    isActive,
  }) {
    return http.post('/connect_session', {
      class_id: classId,
      source,
      target,
      condition_lower: conditionLower,
      condition_upper: conditionUpper,
      isActive,
    });
  },
  putConnectSession({
    lectureId,
    classId,
    isActive,
  }) {
    return http.put(`/connect_session/${classId}/${lectureId}`, { // eslint-disable-line
      isActive,
    });
  },
  getConnectSession({
    id,
  }) {
    return http.get(`/connect_session/${id}`);
  },
  getConnectSessionClass({
    id,
  }) {
    return http.get(`/connect_session/class/${id}`);
  },
  deleteConnectSession({
    id,
  }) {
    return http.delete(`/connect_session/${id}`);
  },
  getLectureGroupMove({
    lectureId,
  }) {
    return http.get(`/lectures/get/groupMove/${lectureId}`);
  },
  postLectureGroupMove({
    lectureId,
    moveId,
  }) {
    return http.post(`/lectures/post/groupMove/${lectureId}/${moveId}`);
  },
  putLectureGroupMove({
    lectureId,
    moveId,
  }) {
    return http.put(`/lectures/put/groupMove/${lectureId}/${moveId}`);
  },
  postLectureScreenConcentration(
    lectureId,
    lectureScreenConcentration,
    start,
    end
  ) {
    return http.post(`/lectures/screenConcentration`, {
      lectureId,
      lectureScreenConcentration,
      start,
      end
    });
  },
  getLectureLiveType({
    lectureId,
  }) {
    return http.get(`/lectures/get/liveType/${lectureId}`);
  },
  putLectureLiveType({
    lectureId,
    type,
  }) {
    return http.put(`/lectures/put/liveType/${lectureId}/${type}`);
  },
  postLectureLiveType({
    lectureId,
    type,
  }) {
    return http.post(`/lectures/post/liveType/${lectureId}/${type}`);
  },
  getLectureProgramming({
    lectureId,
  }) {
    return http.get(`/lectures/get/programming/${lectureId}`);
  },
  putLectureProgramming({
    lectureId,
    type,
  }) {
    return http.put(`/lectures/put/programming/${lectureId}/${type}`);
  },
  postLectureProgramming({
    lectureId,
    type,
  }) {
    return http.post(`/lectures/post/programming/${lectureId}/${type}`);
  },
  postSnapshotStatus({
    lectureId,
    status,
  }) {
    return http.post(`/lectures/snapshotStatus`, {
      lectureId,
      status,
    });
  },
  getSnapshotStatus({
    lectureId,
  }) {
    return http.get('/lectures/snapshotStatus', {
      params: {
        lectureId,
      },
    });
  },
};

