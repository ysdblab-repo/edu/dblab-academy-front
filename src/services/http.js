import axios from 'axios';
import { cacheAdapterEnhancer, throttleAdapterEnhancer } from 'axios-extensions';
import utils from '../utils';
import { baseUrl } from './config';
// import { baseUrl, faceUrl } from './config';

const axiosConfig = {
  headers: {
    'Cache-Control': 'no-cache',
  },
  adapter: throttleAdapterEnhancer(
    cacheAdapterEnhancer(axios.defaults.adapter, false),
  ),
};

const isDev = true; // TODO: replace
if (isDev) {
  axiosConfig.baseURL = baseUrl;
}

const instance = axios.create(axiosConfig);
instance.interceptors.request.use(
  function (config) {
    config.headers['x-access-token'] = utils.getJwtFromLocalStorage();
    return config;
  },
);

export default instance;
