import Vue from 'vue';
import Vuex from 'vuex';

import auth from './authStore';
import layout from './layoutStore';
import sc from './scStore';
import lc from './lcStore';
import scItem from './scItemStore';
import lcItem from './lcItemStore';
import sibalClassIsReservedWord from './classStore';
import NNclass from './NNclassStore';
import analysis from './analysisStore';
import board from './boardStore';
import kMap from './kMapStore';
import report from './reportStore';
import keyword from './keywordStore';
import lectureEditTab from './lectureEditTabStore';
import allowedProgram from './allowedProgramStore';
import grading from './gradingStore';
import studentQuestion from './studentQuestionStore';

import MasterUni from './MasterUniStore';
import MasterDept from './MasterDeptStore';
import MasterTeacher from './MasterTeacherStore';
import reporting from './classReportStore';
import attendance from './attendanceStore';

Vue.use(Vuex);
const store =  new Vuex.Store({
  modules: {
    auth,
    layout,
    scItem,
    sc,
    lc,
    lcItem,
    class: sibalClassIsReservedWord,
    NNclass,
    analysis,
    board,
    kMap,
    lectureEditTab,
    keyword,
    report,
    allowedProgram,
    grading,
    MasterUni,
    MasterDept,
    MasterTeacher,
    studentQuestion,
    reporting,
    attendance,
  },
});

export default store;
// export function resetState() {
//   store.replaceState(JSON.parse(JSON.stringify(initialStateCopy)))
// };
// const initialStateCopy = JSON.parse(JSON.stringify({
//   ...store.state,
//   auth: {
//     jwt: "",
//     locale: "ko",
//     redirectionTimeoutId: null,
//     temporalJwt: "",
//   }
// }));
