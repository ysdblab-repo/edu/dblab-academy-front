import _ from 'lodash';
import classReportService from '../services/classReportService';

export default {
  namespaced: true,
  state: {
    curState: 0,
    curStep: 0,
    curLectureId: null,
    lectureList: [],
    curLectureItems: [],  // question list
    curLectureItems2: [], // survey list
    lectureResult: [],
    studentResult: [],
    studentReportResult: [],
    lectureTotalReportResult: [],
    studentTotalReportResult: null,
    studentReportStd: null,
    lectureReportStd: null,
  },
  mutations: {
    updateCurLectureId(state, { lectureId }) {
      state.curLectureId = lectureId;
    },
    updateLectureList(state, { result }) {
      state.lectureList = result;
    },
    updateLectureItemList(state, { result }) {
      state.curLectureItems.splice(0, state.curLectureItems.length);
      state.curLectureItems = result;
    },
    updateLectureItemList2(state, { result }) {
      state.curLectureItems2.splice(0, state.curLectureItems2.length);
      state.curLectureItems2 = result;
    },
    updateCurStep(state, { index }) {
      state.curStep = index;
    },
    updateCurState(state, { index }) {
      state.curState = index;
    },
    updateStudentList(state, { result }) {
      state.studentResult = result;
    },
    updateStudentReportResult(state, { result }) {
      state.studentReportResult = result;
    },
    updateLectureTotalReportResult(state, { result }) {
      state.lectureTotalReportResult = result;
    },
    updateStudentTotalReportResult(state, { result }) {
      state.studentTotalReportResult = result;
    },
    updateLectureReportStd(state, { result }) {
      state.lectureReportStd = result;
    },
    updateStudentReportStd(state, { result }) {
      state.studentReportStd = result;
    },
  },
  actions: {
    async getLectureList({ commit }, { classId }) {
      const result = await classReportService.getLectureList({ classId });
      commit('updateLectureList', { result: result.data });
    },
    async getLectureItemList({ commit }, { lectureId }) {
      const result = await classReportService.getLectureItems({ lectureId });
      // alert(`lecture item - ${JSON.stringify(result.data)}`)
      commit('updateLectureItemList', { result: result.data.questionResult });
      commit('updateLectureItemList2', { result: result.data.surveyResult });
    },
    async postLectureResult({ commit }, { lectureId, result, type }) {
      const studentResult = await classReportService.postLectureResult({ lectureId, result, type });
      commit('updateStudentList', { result: studentResult.data });
    },
    async getClassResult({ commit }, { classId, type }) {
      const totalResult = await classReportService.getClassResult({ classId, type });
      // alert(JSON.stringify(totalResult.data));
      const lectureResult = totalResult.data.lectureResult;
      const studentResult = totalResult.data.studentResult;
      const userResult = totalResult.data.userResult;
      const istudent = _.groupBy(studentResult, 'student_id');

      const students = [];
      const lecturesAgg = {};
      const lectureName = {};
      const studentsAgg = {};
      for (let i = 0; i < lectureResult.length; i += 1) {
        lecturesAgg[lectureResult[i].lecture_id] = 0;
        lectureName[lectureResult[i].lecture_id] = lectureResult[i].name;
      }
      // console.log(`lecture init - ${JSON.stringify(lecturesAgg)}`);
      for (let i = 0; i < userResult.length; i += 1) {
        const items = istudent[userResult[i].user_id];
        const student = {
          name: userResult[i].name,
          student_id: userResult[i].user_id,
        };
        studentsAgg[userResult[i].user_id] = 0;
        for (let j = 0; j < lectureResult.length; j += 1) { // 임의로 초기화
          student[lectureResult[j].lecture_id] = 0;
        }
        if (items === undefined) {
          continue; // eslint-disable-line
        }
        for (let j = 0; j < items.length; j += 1) {
          student[items[j].lecture_id] += items[j].score;
          studentsAgg[userResult[i].user_id] += items[j].score;
          lecturesAgg[items[j].lecture_id] += items[j].score;
        }
        students.push(student);
      }
      const keys = Object.keys(lecturesAgg);
      const tmpResult = [];
      for (let i = 0; i < keys.length; i += 1) {
        const obj = { lecture_id: keys[i], sum: lecturesAgg[keys[i]], name: lectureName[keys[i]] };
        tmpResult.push(obj);
      }
      const lectureStd = {};
      for (let i = 0; i < keys.length; i += 1) {
        lectureStd[`${keys[i]}`] = 0;
      }
      for (let i = 0; i < keys.length; i += 1) {
        const lectureAvg = lecturesAgg[keys[i]] / students.length;
        let lectureDiffSum = 0;
        for (let j = 0; j < students.length; j += 1) {
          const sum = students[j][keys[i]];
          let diff = lectureAvg - sum;
          if (diff < 0) { diff = -diff; }
          lectureDiffSum += diff;
        }
        lectureStd[`${keys[i]}`] = parseFloat((lectureDiffSum / keys.length).toFixed(2));
      }
      // let tmp = Object.values(studentsAgg);
      // let studentSum = 0;
      // let studentAvg = 0;
      const studentStd = {};
      for (let i = 0; i < students.length; i += 1) {
        let diffSum1 = 0;
        const studentAvg = studentsAgg[students[i].student_id] / tmpResult.length;
        for (let j = 0; j < keys.length; j += 1) {
          let diff = studentAvg - students[i][keys[j]];
          if (diff < 0) { diff = -diff; }
          diffSum1 += diff;
        }
        studentStd[students[i].student_id] = parseFloat((diffSum1 / tmpResult.length).toFixed(2));
      }
      // console.log(`student std result - ${JSON.stringify(studentStd)}`);
      // console.log(`lecture std result - ${JSON.stringify(lectureStd)}`);
      // alert(JSON.stringify(tmpResult));
      // alert(`student - ${JSON.stringify(students)}`);
      // alert(`student agg - ${JSON.stringify(studentsAgg)}`);
      // alert(`lecture agg - ${JSON.stringify(tmpResult)}`);
      commit('updateLectureReportStd', { result: lectureStd });
      commit('updateStudentReportStd', { result: studentStd });
      commit('updateStudentReportResult', { result: students });
      commit('updateStudentTotalReportResult', { result: studentsAgg });
      commit('updateLectureTotalReportResult', { result: tmpResult });
    },
  },
};
