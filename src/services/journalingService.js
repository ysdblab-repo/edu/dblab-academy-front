import http from './http';

export default {
  getJournalingClass({
    classId,
  }) {
    return http.get(`/journalings3?classId=${classId}`);
  },
  getJournalingLecture({
    classId,
    lectureId,
  }) {
    return http.get(`/journalings3?classId=${classId}&lectureId=${lectureId}`);
  },
  getJournalingItem({
    classId,
    lectureId,
    itemId,
  }) {
    return http.get(`/journalings3?classId=${classId}&lectureId=${lectureId}&itemId=${itemId}`);
  },
  getJournalingView({
    classId,
    lectureId,
    itemId,
  }) {
    if (itemId !== undefined) {
      return http.get(`/journalings3?classId=${classId}&lectureId=${lectureId}&itemId=${itemId}&view=student`);
    } else if (lectureId !== undefined) {
      return http.get(`/journalings3?classId=${classId}&lectureId=${lectureId}&view=student`);
    }
    return http.get(`/journalings3?classId=${classId}&view=student`);
  },
  postJournalingItem({
    classId,
    lectureId,
    itemResult,
  }) {
    return http.post('/journaling4/question_item', {
      class_id: classId,
      lecture_id: lectureId,
      item_result: itemResult,
    });
  },
  postJournalingLecture({
    classId,
    lectureId,
    lectureResult,
  }) {
    return http.post('/journaling4/question_lecture', {
      class_id: classId,
      lecture_id: lectureId,
      lecture_result: lectureResult,
    });
  },

  postJournalingPreview({
    classId,
    lectureId,
    previewResult,
  }) {
    return http.post('/journaling4/question_preview', {
      class_id: classId,
      lecture_id: lectureId,
      preview_result: previewResult,
    });
  },

  postJournalingMain({
    classId,
    lectureId,
    currentResult,
  }) {
    return http.post('/journaling4/question_current', {
      class_id: classId,
      lecture_id: lectureId,
      current_result: currentResult,
    });
  },

  postJournalingReview({
    classId,
    lectureId,
    reviewResult,
  }) {
    return http.post('/journaling4/question_review', {
      class_id: classId,
      lecture_id: lectureId,
      review_result: reviewResult,
    });
  },
  postJournalingKeyword({
    classId,
    lectureId,
    keywordResult,
  }){
    return http.post('/journaling4/keyword_journaling', {
      class_id: classId,
      lecture_id: lectureId,
      keyword_result: keywordResult,
    });
  },
  getJournalingKeyword({
    classId,
    lectureId,
  }){
    return http.get('/journaling4/keyword_journaling', {

      params:{
        class_id: classId,
        lecture_id: lectureId,
      },
    });
  },
};
