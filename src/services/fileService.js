import http from './http';

export default {
  deleteFile({
    fileGuid,
  }) {
    return http.delete(`/files/${fileGuid}`);
  },
  deleteFileSqlite({
    fileGuid,
    questionId,
  }) {
    return http.delete(`/files/${fileGuid}/sqlite/${questionId}`);
  },
  previousSqliteFile() {
    return http.get('/files/previous-sqlite-file');
  },
  getFileName({
    clientPath,
  }) {
    const encodedClientPath = encodeURIComponent(clientPath);
    return http.get(`files/name/${encodedClientPath}`);
  },
  getFile({
    guid,
  }) {
    return http.get(`files/${guid}`, { responseType: 'blob' });
  },
  getFileHaarcascade({
    name,
  }) {
    return http.get(`files/haarcascades/${name}`);
  }
};
