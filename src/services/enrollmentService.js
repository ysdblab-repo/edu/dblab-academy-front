import http from './http';

export default {
  postEnrollment({
		class_id,
  }) {
	return http.post('/enrollments/apply', { class_id });
  },
  deleteEnrollment({
	class_id,
  }) {
	return http.delete(`/enrollments/apply/${class_id}`);
  },
  checkEnrollmentTime({ class_id }) {
	return http.get(`/enrollments/check_time/${class_id}`);
  },
  getEnrollment() {
	return http.get('/enrollments/apply/');
  },
  getEnrollmentByClassId({
	class_id
  }) {
	return http.get(`/enrollments/waiting/${class_id}`)
  },
  permitEnrollment({
	class_id,
	user_id,
  }) {
	return http.put(`/enrollments/permit/${class_id}/${user_id}`);
  },
  rejectEnrollment({
	class_id,
	user_id,
  }) {
	return http.put(`/enrollments/refuse/${class_id}/${user_id}`);
  },
  postEnrollmentExcel({
	class_id,
	user_id,
  }) {
	return http.post('/enrollments/excel', { class_id, user_id });
  },
  deleteCancelPreview({
    class_id,
  }) {
    return http.delete(`/enrollments/cancel_preview/${class_id}`);
  },
};
