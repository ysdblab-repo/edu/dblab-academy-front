// import Test from '../components/TestComponent';
import Profile from '../components/pages/Profile';
import ProfileEdit from '../components/pages/ProfileEdit';
import TeacherClassIndex from '../components/pages/TeacherClassIndex';
import NNTeacherClassIndex from '../components/pages/NNTeacherClassIndex';
import TeacherClassManage from '../components/pages/TeacherClassManage'
import NNTeacherClassNew from '../components/pages/NNTeacherClassNew';
import TeacherClassManageEnrollment from '../components/pages/TeacherClassManageEnrollment';
import NNTeacherClassShow from '../components/pages/NNTeacherClassShow';
import NNTeacherLectureNew from '../components/pages/NNTeacherLectureNew';
import NNTeacherLectureManage from '../components/pages/NNTeacherLectureManage';
import NNTeacherLectureLive from '../components/pages/NNTeacherLectureLive';
import NNStudentClassIndex from '../components/pages/NNStudentClassIndex';
import NNStudentClassShow from '../components/pages/NNStudentClassShow';
import TeacherLectureNew from '../components/pages/TeacherLectureNew';
import TeacherLectureLive from '../components/pages/TeacherLectureLive';
import NNStudentLectureMultiLive from '../components/pages/StudentLectureMultiLive';
import NNStudentLectureLive from '../components/pages/NNStudentLectureLive';
// import TeacherClassNew from '../components/pages/TeacherClassNew'; // season 0
import TeacherClassEdit from '../components/pages/TeacherClassEdit';
import TeacherClassEvaluation from '../components/pages/TeacherClassEvaluation';
import TeacherClassScoring from '../components/pages/TeacherClassScoring';
// import wordCloudExample from '../components/partials/wordCloudExample';
import StudentClassJournal from '../components/pages/NNStudentClassJournal';
import TeacherClassJournal from '../components/pages/TeacherClassJournal';
import NNTeacherClassJournal from '../components/pages/NNTeacherClassJournal';
import StudentLectureJournal from '../components/pages/StudentLectureJournal';
import TeacherLectureJournal from '../components/pages/TeacherLectureJournal';
import ClassQuestionAnswer from '../components/pages/ClassQuestionAnswer';
import NNClassQuestionAnswer from '../components/pages/NNClassQuestionAnswer';
import NNClassNotice from '../components/pages/NNClassNotice';
import ClassQuestionAnswerWrite from '../components/pages/ClassQuestionAnswerWrite';
import NNClassQuestionAnswerWrite from '../components/pages/NNClassQuestionAnswerWrite';
import NNClassNoticeWrite from '../components/pages/NNClassNoticeWrite';
import ClassQuestionAnswerDetail from '../components/pages/ClassQuestionAnswerDetail';
import NNClassQuestionAnswerDetail from '../components/pages/NNClassQuestionAnswerDetail';
// import test from '../components/partials/test';
import StudentClassReport from '../components/pages/StudentReport';
import TeacherClassReport from '../components/pages/TeacherReport';
import TeacherClassGrading from '../components/pages/TeacherClassGrading';
import NNTeacherClassGrading from '../components/pages/NNTeacherClassGrading';
import TeacherClassReporting from '../components/pages/TeacherClassReporting';
import NNTeacherClassReporting from '../components/pages/NNTeacherClassReporting';
import TeacherClassGradingQuestion from '../components/pages/TeacherClassGradingQuestion';
import TeacherClassGradingSurvey from '../components/pages/TeacherClassGradingSurvey';
import TeacherClassGradingPractice from '../components/pages/TeacherClassGradingPractice';
import StudentClassGrade from '../components/pages/StudentClassGrade';
import StudentClassLectureGrade from '../components/pages/StudentClassLectureGrade';
import StudentClassAttendance from '../components/pages/StudentClassAttendance';
import Bank from '../components/pages/Bank';
import NClassFromBank from '../components/pages/NClassFromBank';
import NLectureFromBank from '../components/pages/NLectureFromBank';
import NLectureItemFromBank from '../components/pages/NLectureItemFromBank';
import LectureItemConnect from '../components/pages/LectureItemConnect';
// import LectureItemGroup from '../components/pages/LectureItemGroup';
import LectureItemSetting from '../components/pages/LectureItemSetting';
import TeacherLectureLiveItemShow from '../components/pages/TeacherLectureLiveItemShow';
import TeacherLectureLiveStudentStatus from '../components/pages/TeacherLectureLiveStudentStatus';
import TeacherClassAttendance from '../components/pages/TeacherClassAttendance';

import ViewUni from '../components/pages/ViewUni';
// import ViewUniUpdate from '../components/pages/ViewUniUpdate';
import ViewDept from '../components/pages/ViewDept';
import ViewTeacher from '../components/pages/ViewTeacher';
// import ViewTeacherAll from '../components/pages/ViewTeacherAll';
import ViewClass from '../components/pages/ViewClass';
import ViewDetailClass from '../components/pages/ViewClassAll';
import ViewStudent from '../components/pages/ViewStudent';
import ViewStudentScore from '../components/pages/ViewStudentScore';
import ViewDetailStudent from '../components/pages/ViewDetailStudent';
import ViewBank from '../components/pages/ViewBank';
import RegisterUni from '../components/pages/RegisterUni';

// import RegisterUniEdit from '../components/pages/RegisterUni';
import RegisterUniSuccess from '../components/pages/RegisterUniSuccess';
import RegisterDept from '../components/pages/RegisterDept';
import RegisterDeptSuccess from '../components/pages/RegisterDeptSuccess';
import RegisterTeacher from '../components/pages/RegisterTeacher';
import RegisterTeacherSuccess from '../components/pages/RegisterTeacherSuccess';
import RegisterClass from '../components/pages/NNRegisterClass';
import RegisterClassSuccess from '../components/pages/RegisterClassSuccess';
import RegisterStudent from '../components/pages/RegisterStudent';
import RegisterStudentSuccess from '../components/pages/RegisterStudentSuccess';
import RegisterBank from '../components/pages/RegisterBank';
import RegisterBankSuccess from '../components/pages/RegisterBankSuccess';

import StudentLectureQuiz from '../components/pages/StudentLectureQuiz';

import StudentEnrollClass from '../components/pages/StudentEnrollClass';

import RealtimeWebcamLecture from '../components/pages/RealtimeWebcamLecture';
import RealtimeWebcamLectureStudent from '../components/pages/RealtimeWebcamLectureStudent';
import RealtimeWebcamLectureStudentPrivate from '../components/pages/RealtimeWebcamLectureStudentPrivate';
import RealtimeVideoDiscussion from '../components/pages/RealtimeVideoDiscussion';
// import RealtimeVideoDiscussion2 from '../components/pages/RealtimeVideoDiscussion2';
import RealtimeVideoDiscussion3 from '../components/pages/RealtimeVideoDiscussion3';
import RealtimeVideoDiscussion4 from '../components/pages/RealtimeVideoDiscussion4';

import ReqCreateClass from '../components/pages/ReqCreateClass';

import SGroup from '../components/pages/SGroup';
import SGroupRoomEdit from '../components/pages/SGroupRoomEdit';
import SGroupRoom from '../components/pages/SGroupRoom';
import SGroupRoom2 from '../components/pages/SGroupRoom2';
import SGroupRoom3 from '../components/pages/SGroupRoom3';
import SGroupRoom4 from '../components/pages/SGroupRoom4';
import OpenSpace from '../components/pages/OpenSpace';
import InstitutionList from '../components/pages/InstitutionList';
import NNClassNoticeDetail from '../components/pages/NNClassNoticeDetail';
import ErrorManage from '../components/pages/ErrorManage';
import NNClassNoticeModify from '../components/pages/NNClassNoticeModify';
import v4_landing from '../components/pages/v4_landing';
import utils from '../utils';
import store from '../stores';


export default function authRoutes(root) {
  return [
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      meta: {
        title: 'Profile', // TODO: import from i18n
      },
    },
    {
      path: '/',
      name: 'LandingPage',
      component: v4_landing,
      meta: {
        title: 'LandingPage',
      }
    },
    {
      path: '/profileEdit',
      name: 'ProfileEdit',
      component: ProfileEdit,
      meta: {
        title: 'ProfileEdit',
      },
    },
    {
      path: '/teacher/class/',
      name: 'TeacherClass',
      component: TeacherClassIndex,
      meta: {
        title: 'Teacher Class Index', // TODO: import from i18n
      },
    },
    {
      path: '/teacher/NNclass/',
      name: 'NNTeacherClassIndex',
      component: NNTeacherClassIndex,
      meta: {
        title: 'Teacher Class Index2', // TODO: import from i18n
      },
    },
    {
      path: '/teacher/class/Manage',
      name: 'TeacherClassManage',
      component: TeacherClassManage,
      meta: {
        title: 'Teacher Class Manage', // TODO: import from i18n
      },
    },
    {
      alias: '/teacher/NNclass/:classId/edit',
      path: '/teacher/NNclass/new',
      name: 'NNTeacherClassNew',
      component: NNTeacherClassNew,
      meta: {
        title: 'ddd',
      },
    },
    {
      path: '/teacher/NNclass/:classId/manage/enrollment',
      name: 'TeacherClassManageEnrollment',
      component: TeacherClassManageEnrollment,
      meta: {
        title: '수강관리',
      },
    },
    {
      path: '/teacher/NNlecture/new',
      name: 'NNTeacherLectureNew',
      component: NNTeacherLectureNew,
      meta: {
        title: 'Teacher Lecture New',
      },
    },
    {
      path: '/teacher/NNclass/newfrombank',
      name: 'NClassFromBank',
      component: NClassFromBank,
      meta: {
        title: 'New Class From Bank',
      },
    },
    {
      path: '/teacher/NNlecture/newfrombank',
      name: 'NLectureFromBank',
      component: NLectureFromBank,
      meta: {
        title: 'New Lecture From Bank',
      },
    },
    {
      path: '/teacher/NNlecture/newitemfrombank',
      name: 'NLectureItemFromBank',
      component: NLectureItemFromBank,
      meta: {
        title: 'New Lecture Item From Bank',
      },
    },
    {
      path: '/teacher/NNlecture/itemconnection',
      name: 'LectureItemConnect',
      component: LectureItemConnect,
      meta: {
        title: 'Lecture Item Connect',
      },
    },
    // {
    //   path: '/teacher/NNlecture/itemgroup',
    //   name: 'LectureITemGroup',
    //   component: LectureItemGroup,
    //   meta: {
    //     title: 'Lecture Item Grouping',
    //   },
    // },
    {
      path: '/teacher/NNlecture/itemsetting',
      name: 'LectureITemSetting',
      component: LectureItemSetting,
      meta: {
        title: 'Lecture Item Setting',
      },
    },
    {
      path: '/teacher/NNlecture/:lectureId/manage',
      name: 'NNTeacherLectureManage',
      component: NNTeacherLectureManage,
      meta: {
        title: 'Teacher Lecture Manage',
      },
    },
    {
      path: '/teacher/NNlecture/:lectureId/live',
      name: 'NNTeacherLectureLive',
      component: NNTeacherLectureLive,
      meta: {
        title: 'Teacher Lecture Live',
      },
    },
    {
      path: '/teacher/NNlecture/:lectureId/itemList',
      name: 'TeacherLectureLiveItemShow',
      component: TeacherLectureLiveItemShow,
      meta: {
        title: 'Teacher Lecture Live Item Show',
      },
    },
    {
      path: '/teacher/NNlecture/:lectureId/studentStatus',
      name: 'TeacherLectureLiveStudentStatus',
      component: TeacherLectureLiveStudentStatus,
      meta: {
        title: 'Teacher Lecture Live Student Status',
      },
    },
    /*  // season 0
    {
      path: '/teacher/class/new',
      name: 'TeacherClassNew',
      component: TeacherClassNew,
      meta: {
        title: 'Teacher Class New', // TODO: import from i18n
      },
    },
    */
    {
      path: '/teacher/NNclass/:classId',
      name: 'NNTeacherClassShow',
      component: NNTeacherClassShow,
      meta: {
        title: 'Teacher Class Show', // TODO: import from i18n
      },
    },
    {
      path: '/teacher/class/:classId/edit',
      name: 'TeacherClassEdit',
      component: TeacherClassEdit,
      meta: {
        title: 'Teacher Class Edit', // TODO: import from i18n
      },
    },
    {
      path: '/teacher/class/:classId/evaluation',
      name: 'TeacherClassEvaluation',
      component: TeacherClassEvaluation,
      meta: {
        title: 'Teacher Class Evaluation',
      },
    },
    {
      path: '/teacher/class/:classId/scoring',
      name: 'TeacherClassScoring',
      component: TeacherClassScoring,
      meta: {
        title: 'Teacher Class Scoring',
      },
    },
    {
      path: '/teacher/class/:classId/grading',
      name: 'TeacherClassGrading',
      component: TeacherClassGrading,
      meta: {
        title: 'Teacher Class Grading',
      },
    },
    {
      path: '/teacher/class/:classId/NNgrading',
      name: 'NNTeacherClassGrading',
      component: NNTeacherClassGrading,
      meta: {
        title: 'Teacher Class Grading',
      },
    },
    {
      path: '/teacher/class/:classId/attendance',
      name: 'TeacherClassAttendance',
      component: TeacherClassAttendance,
      meta: {
        title: 'Teacher Class Attendance',
      },
    },

    {
      path: '/student/class/:classId/attendance/:userId',
      name: 'StudentClassAttendance',
      component: StudentClassAttendance,
      meta: {
        title: 'Student Class Attendance',
      },
    },
    {
      path: '/teacher/class/:classId/reporting',
      name: 'TeacherClassReporting',
      component: TeacherClassReporting,
      meta: {
        title: 'Teacher Class Reporting',
      },
    },
    {
      path: '/teacher/class/:classId/NNreporting',
      name: 'NNTeacherClassReporting',
      component: NNTeacherClassReporting,
      meta: {
        title: 'Teacher Class Reporting',
      },
    },
    {
      path: '/teacher/class/:classId/grading/question/:itemId',
      name: 'TeacherClassGradingQuestion',
      component: TeacherClassGradingQuestion,
      meta: {
        title: 'Teacher Class Grading Question',
      },
    },
    {
      path: '/teacher/class/:classId/grading/survey/:itemId',
      name: 'TeacherClassGradingSurvey',
      component: TeacherClassGradingSurvey,
      meta: {
        title: 'Teacher Class Grading Survey',
      },
    },
    {
      path: '/teacher/class/:classId/grading/practice/:itemId',
      name: 'TeacherClassGradingPractice',
      component: TeacherClassGradingPractice,
      meta: {
        title: 'Teacher Class Grading Practice',
      },
    },
    {
      path: '/student/class/:classId/grade',
      name: 'StudentClassGrade',
      component: StudentClassGrade,
      meta: {
        title: 'Student Class Grade',
      },
    },
    {
      path: '/student/class/:classId/grade/lecture/:lectureId',
      name: 'StudentClassLectureGrade',
      component: StudentClassLectureGrade,
      meta: {
        title: 'Student Lecture Grade',
      },
    },
    {
      path: '/class/:classId/board/:pageNum',
      name: 'ClassQuestionAnswer',
      component: ClassQuestionAnswer,
      meta: {
        title: 'Class Q&A',
      },
    },
    {
      path: '/class/:classId/NNboard/:pageNum',
      name: 'NNClassQuestionAnswer',
      component: NNClassQuestionAnswer,
      meta: {
        title: 'NN Class Q&A',
      },
    },
    {
      path: '/class/:classId/NNNotice/:pageNum',
      name: 'NNClassNotice',
      component: NNClassNotice,
      meta: {
        title: 'NN Class Notice',
      },
    },
    {
      path: '/class/:classId/NNClassNoticeModify/:noticeId',
      name: 'NNClassNoticeModify',
      component: NNClassNoticeModify,
      meta: {
        title: 'NN Class Notice modify',
      },
    },
    {
      path: '/class/:classId/boardwrite',
      name: 'ClassQuestionAnswerWrite',
      component: ClassQuestionAnswerWrite,
      meta: {
        title: 'Class Q&A Write',
      },
    },
    {
      path: '/class/:classId/NNboardwrite',
      name: 'NNClassQuestionAnswerWrite',
      component: NNClassQuestionAnswerWrite,
      meta: {
        title: 'Class Q&A Write',
      },
    },
    {
      path: '/class/:classId/NNnoticewrite',
      name: 'NNClassNoticeWrite',
      component: NNClassNoticeWrite,
      meta: {
        title: 'Class Notice Write',
      },
    },
    {
      path: '/class/:classId/NNnoticedetail/:noticeId',
      name: 'NNClassNoticeDetail',
      component: NNClassNoticeDetail,
      meta: {
        title: 'Class Notice Detail',
      },
    },
    {
      path: '/class/:classId/boarddetail/:boardId',
      name: 'ClassQuestionDetail',
      component: ClassQuestionAnswerDetail,
      meta: {
        title: 'Class Q&A Detail',
      },
    },
    {
      path: '/class/:classId/NNboarddetail/:boardId',
      name: 'NNClassQuestionDetail',
      component: NNClassQuestionAnswerDetail,
      meta: {
        title: 'Class Q&A Detail',
      },
    },
    {
      path: '/teacher/lecture/:scId/edit',
      name: 'TeacherLectureNew',
      component: TeacherLectureNew,
      meta: {
        title: 'Teacher New Lecture', // TODO: import from i18n
      },
    },
    {
      path: '/teacher/lecture/:scId/live',
      name: 'TeacherLectureLive',
      component: TeacherLectureLive,
      meta: {
        title: 'Teacher New Live', // TODO: import from i18n
      },
    },
    {
      path: '/student/NNclass/',
      name: 'NNStudentClassIndex',
      component: NNStudentClassIndex,
      meta: {
        title: 'Student Class Index 2', // TODO: import from i18n
      },
    },
    {
      path: '/student/enroll/class',
      name: 'StudentEnrollClass',
      component: StudentEnrollClass,
      meta: {
        title: 'Student Class Enrollment', // TODO: import from i18n
      },
    },
    {
      path: '/student/NNclass/:classId',
      name: 'NNStudentClassShow',
      component: NNStudentClassShow,
      meta: {
        title: 'NN Student Class Show', // TODO: import from i18n
      },
    },
    /*
    {
      path: '/student/lecture/:scId/:order',
      name: 'StudentLectureLive',
      component: StudentLectureLive,
      meta: {
        title: 'Student Lecture Live',
      },
    },
    */
    
    {
      path: '/student/NNlecture/:lectureId/:order',
      name: 'NNStudentLectureLive',
      component: NNStudentLectureMultiLive,
      //component: NNStudentLectureLive,
      meta: {
        title: 'Student Lecture Live',
      },
    },
    {
      path: '/student/class/:classId/:userId/journal',
      name: 'PPStudentClassJournal',
      component: StudentClassJournal,
      meta: {
        title: 'Student Class Journal',
      },
    },
    {
      path: '/teacher/class/:classId/journal',
      name: 'TeacherClassJournal',
      component: TeacherClassJournal,
      meta: {
        title: 'Teacher Class Journal',
      },
    },
    {
      path: '/teacher/class/:classId/NNjournal',
      name: 'NNTeacherClassJournal',
      component: NNTeacherClassJournal,
      meta: {
        title: 'Teacher Class Journal',
      },
    },
    {
      path: '/student/class/:classId/journal',
      name: 'StudentClassJournal',
      component: StudentClassJournal,
      meta: {
        title: 'Student Class Journal',
      },
    },
    {
      path: '/student/lecture/:lectureId/:userId/journal',
      name: 'StudentLectureJournal',
      component: StudentLectureJournal,
      meta: {
        title: 'Student Lecture Journal',
      },
    },
    {
      path: '/teacher/lecture/:lectureId/journal',
      name: 'TeacherLectureJournal',
      component: TeacherLectureJournal,
      meta: {
        title: 'Teacher Lecture Journal',
      },
    },
    {
      path: '/report/student/:userId/class/:classId/',
      name: 'StudentClassReport',
      component: StudentClassReport,
      meta: {
        title: '학생 세션 보고서 페이지',
      },
    },

    {
      path: '/notice/teacher/:userId/class/:classId/',
      name: 'TeacherClassReport',
      component: TeacherClassReport,
      meta: {
        title: '강사 세션 보고서 페이지',
      },
    },

    {
      path: '/bank',
      name: 'Bank',
      component: Bank,
      meta: {
        title: '세션 은행',
      },
    },
    {
      path: '/view/uni',
      name: 'ViewUni',
      component: ViewUni,
      meta: {
        title: '조회하기-대학',
      },
    },
    {
      path: '/view/dept',
      name: 'ViewDept',
      component: ViewDept,
      meta: {
        title: '관리자 학과 조회',
      },
    },
    {
      path: '/view/teacher',
      name: 'ViewTeacher',
      component: ViewTeacher,
      meta: {
        title: '관리자 강사 조회',
      },
    },
    {
      path: '/view/class',
      name: 'ViewClass',
      component: ViewClass,
      meta: {
        title: '관리자 주제 및 세션 조회',
      },
    },
    {
      path: '/view/:code/detail',
      name: 'ViewDetailClass',
      component: ViewDetailClass,
      meta: {
        title: '관리자 주제 상세 보기',
      },
    },
    {
      path: '/view/student',
      name: 'ViewStudent',
      component: ViewStudent,
      meta: {
        title: '관리자 학생 조회',
      },
    },
    {
      path: '/view/studentScore',
      name: 'ViewStudentScore',
      component: ViewStudentScore,
      meta: {
        title: '관리자 학생성적표 조회',
      },
    },
    {
      path: '/student/:code/detail',
      name: 'ViewDetailStudent',
      component: ViewDetailStudent,
      meta: {
        title: '관리자 학생 개인 성적표 조회',
      },
    },
    {
      path: '/view/bank',
      name: 'ViewBank',
      component: ViewBank,
      meta: {
        title: '관리자 세션은행 조회',
      },
    },
    {
      path: '/register/uni',
      name: 'RegisterUni',
      component: RegisterUni,
      meta: {
        title: '관리자 대학 등록',
      },
    },
    {
      path: '/register/uni/success',
      name: 'RegisterUniSuccess',
      component: RegisterUniSuccess,
      meta: {
        title: '관리자 대학 등록 성공',
      },
    },
    {
      alias: '/:code/edit',
      path: '/register/uni/edit',
      name: 'RegisterUniEdit',
      component: RegisterUni,
      meta: {
        title: '관리자 대학 수정',
      },
    },
    {
      path: '/register/dept',
      name: 'RegisterDept',
      component: RegisterDept,
      meta: {
        title: '관리자 학과 등록',
      },
    },
    {
      path: '/register/dept/success',
      name: 'RegisterDeptSuccess',
      component: RegisterDeptSuccess,
      meta: {
        title: '관리자 학과 등록 성공',
      },
    },
    {
      alias: '/:unicode/dept/:code/edit',
      path: '/register/dept/edit',
      name: 'RegisterDeptEdit',
      component: RegisterDept,
      meta: {
        title: '관리자 학과 수정',
      },
    },
    {
      path: '/register/teacher',
      name: 'RegisterTeacher',
      component: RegisterTeacher,
      meta: {
        title: '관리자 강사 등록',
      },
    },
    {
      path: '/register/teacher/success',
      name: 'RegisterTeacherSuccess',
      component: RegisterTeacherSuccess,
      meta: {
        title: '관리자 강사 등록 성공',
      },
    },
    {
      alias: '/teacher/:code/edit',
      path: '/register/teacher/edit',
      name: 'RegisterTeacherEdit',
      component: RegisterTeacher,
      meta: {
        title: '관리자 강사 수정',
      },
    },
    {
      path: '/register/class',
      name: 'RegisterClass',
      component: RegisterClass,
      meta: {
        title: '관리자 주제 등록',
      },
    },
    {
      path: '/error/manage',
      name: 'ErrorManage',
      component: ErrorManage,
      meta: {
        title: '오류 관리',
      },
    },
    {
      alias: '/class/:code/edit',
      path: '/register/class/edit',
      name: 'RegisterClassEdit',
      component: RegisterClass,
      meta: {
        title: '관리자 주제 수정',
      },
    },
    {
      path: '/register/class/success',
      name: 'RegisterClassSuccess',
      component: RegisterClassSuccess,
      meta: {
        title: '관리자 주제 등록 성공',
      },
    },
    {
      path: '/register/student',
      name: 'RegisterStudent',
      component: RegisterStudent,
      meta: {
        title: '관리자 학생 등록',
      },
    },
    {
      path: '/register/student/success',
      name: 'RegisterStudentSuccess',
      component: RegisterStudentSuccess,
      meta: {
        title: '관리자 학생 등록 성공',
      },
    },
    {
      alias: '/student/:code/edit',
      path: '/register/student/edit',
      name: 'RegisterStudentEdit',
      component: RegisterStudent,
      meta: {
        title: '관리자 학생 수정',
      },
    },
    {
      path: '/register/bank',
      name: 'RegisterBank',
      component: RegisterBank,
      meta: {
        title: '관리자 세션은행그룹 등록',
      },
    },
    {
      alias: '/bank/:code/edit',
      path: '/register/bank/edit',
      name: 'RegisterBankEdit',
      component: RegisterBank,
      meta: {
        title: '관리자 세션은행그룹 수정',
      },
    },
    {
      path: '/register/bank/success',
      name: 'RegisterBankSuccess',
      component: RegisterBankSuccess,
    },
    {
      path: '/student/NNclass/:classId/quiz/',
      name: 'StudentLectureQuiz',
      component: StudentLectureQuiz,
      meta: {
        title: '관리자 세션은행그룹 등록 성공',
      },
    },
    {
      path: '/webcam/:lectureId/:classId',
      name: 'RealtimeWebcamLecture',
      component: RealtimeWebcamLecture,
      meta: {
        title: '강사와 학생간 화상세션',
      },
    },
    {
      path: '/webcamStudent/:lectureId/:classId',
      name: 'RealtimeWebcamLectureStudent',
      component: RealtimeWebcamLectureStudent,
      meta: {
        title: '강사와 학생간 화상세션',
      },
    },
    {
      path: '/webcamStudentPrivate/:lectureId/:classId',
      name: 'RealtimeWebcamLectureStudentPrivate',
      component: RealtimeWebcamLectureStudentPrivate,
      meta: {
        title: '강사와 학생간 화상세션',
      },
    },
    {
      path: '/realtimeDiscussion/:discussionRoomId',
      name: 'RealtimeVideoDiscussion',
      component: RealtimeVideoDiscussion,
      meta: {
        title: '실시간 화상 토론',
      },
    },
    {
      path: '/SGroup/:lectureId/:classId',
      name: 'SGroup',
      component: SGroup,
      meta: {
        title: '소그룹방',
      },
    },
    {
      path: '/SGroup/:lectureId/:classId/:roomId/edit',
      name: 'SGroupRoomEdit',
      component: SGroupRoomEdit,
      meta: {
        title: '소그룹방 수정',
      },
    },  
    {
      path: '/SGroup/:lectureId/:classId/:roomId',
      name: 'SGroupRoom',
      component: SGroupRoom,
      meta: {
        title: '소그룹방 입장',
      },
    },
    {
      path: '/SGroup2/:classId',
      name: 'SGroupRoom2',
      component: SGroupRoom2,
      meta: {
        title: '소그룹방 입장',
      },
    },
    {
      path: '/SGroup3/:roomId',
      name: 'SGroupRoom3',
      component: SGroupRoom3,
      meta: {
        title: '소그룹방 입장',
      },
    },
    {
      path: '/SGroup4/:roomId',
      name: 'SGroupRoom4',
      component: SGroupRoom4,
      meta: {
        title: '소그룹방 입장',
      },
    },
    {
      path: '/openspace',
      name: 'OpenSpace',
      component: OpenSpace,
      meta: {
        title: '개방 공간',
      }, 
    },
    {
      path: '/student/institution/:affiliationId',
      name: 'InstitutionList',
      component: InstitutionList,
      meta: {
        title: '개방 공간 리스트',
      }, 
    },

    // {
    //   path: '/realtimeDiscussion2/:discussionRoomId',
    //   name: 'RealtimeVideoDiscussion2',
    //   component: RealtimeVideoDiscussion2,
    //   meta: {
    //     title: '실시간 화상 토론',
    //   },
    // },
    {
      path: '/test/:roomId',
      name: 'RealtimeVideoDiscussion3',
      component: RealtimeVideoDiscussion3,
      meta: {
        title: '실시간 화상 토론',
      },
    },
    {
      path: '/realtimeDiscussion4/:discussionRoomId',
      name: 'RealtimeVideoDiscussion4',
      component: RealtimeVideoDiscussion4,
      meta: {
        title: '실시간 화상 토론',
      },
    },
    {
      path: '/affiliationAdmin/reqCreateClass',
      name: 'ReqCreateClass',
      component: ReqCreateClass,
      meta: {
        title: '주제 관리자: 주제 관리 요청 관리',
      },
    },
  ].map((route) => {
    route.path = root + route.path; // eslint-disable-line no-param-reassign
    if (route.alias) {
      route.alias = root + route.alias; // eslint-disable-line no-param-reassign
    }
    if (!route.meta) {
      route.meta = {}; // eslint-disable-line no-param-reassign
    }
    route.meta.auth = true; // eslint-disable-line no-param-reassign
    return route;
  });
}
