import http from './http';

export default {
  getCreateClassReqList({
    affiliation_id,
    status,
    pageNum,
  }) {
    let params = '';
    if (pageNum !== undefined) {
      params += `&pageNum=${pageNum}`;
    }
    return http.get(`/req_create_class?affiliation_id=${affiliation_id}&status=${status}${params}`);
  },
  postAcceptCreateClassReq({
    req_id,
  }) {
    return http.post('/req_create_class/accept', {
      req_id,
    });
  },
  postRejectCreateClassReq({
    req_id,
  }) {
    return http.post('/req_create_class/reject', {
      req_id,
    });
  },
  getAffiliationList({
    user_id,
  }) {
    return http.get(`/user_affiliation/${user_id}`);
  },
  postMainAffiliation({
    user_id,
    affiliation_id,
  }) {
    return http.post(`/user_affiliation/${user_id}/${affiliation_id}`);
  },
  getAffiliationInfo({
    affiliationId,
  }) {
    return http.get(`/affiliation/${affiliationId}`);
  },
  postPermitTeacher({
    userId,
    affiliation_id,
  }) {
    return http.post(`/binding_teacher`, {
      user_id: userId,
      affiliation_id,
    });
  },
};
