import LcItemHandler from './index';
import individualQuestionService from '../../services/individualQuestionService';
import lectureItemService from '../../services/lectureItemService';
import lectureService from '../../services/lectureService';
import utils from '../../utils';
import { forEach } from 'lodash';

export default class IndividualQuestionHandler extends LcItemHandler {
  static async initViewModel(vm) {
    const item = vm.lectureItem.individual_question_infos[0];
    const viewModelInputTail = {
      searchCondition: {
        questionType: item.type,
        keywordSelect: item.keyword,
        difficultyMin: item.difficulty[0],
        difficultyMax: item.difficulty[1],
      },
      groupDataList: {
        groupInfoList: [{
          group: 1,
          type: item.type,
          typeString: utils.convertQuestionType(item.type),
          difficulty: `${item.difficulty[0]}~${item.difficulty[1]}`,
          keywords: item.keyword.join(','),
        }],
        groupQuestionList: [],
      },
      extractionDisable: false,
      lectureId: vm.lecture.lecture_id,
    };
    item.questions.forEach((question) => {
      viewModelInputTail.groupDataList.groupQuestionList.push({
        group: 1,
        name: question.lecture_item.name,
        type: question.type,
        typeString: utils.convertQuestionType(question.type),
        difficulty: question.difficulty,
        // realDifficulty: question.realDifficulty,
        score: question.score,
        data: question,
      });
    });
    if (vm.$refs['individualQuestionEditor'] !== undefined) { // eslint-disable-line
      vm.$refs['individualQuestionEditor'].viewModelInputTail = viewModelInputTail; // eslint-disable-line
      vm.$refs['individualQuestionEditor'].copyInputTail(viewModelInputTail, 0); // eslint-disable-line
      vm.searchFinish = true; // eslint-disable-line
    }
  }
  /* eslint-enable no-param-reassign */

  // 개인화 문항은 특이하게 아이템 생성시 여러개의 아이템을 만들어야 하기 때문에
  // LcItemHandler의 postLcItem을 override하여 새로운 함수를 실행
  static async postLcItem({ lectureId, inputHead, inputBody, inputTail }) {
    if (inputBody.individualType === undefined) {
      return;
    }

    const postLcItemFunc = () => new Promise((resolve) => {
      let forEachCount = 0;
      inputTail.groupDataList.groupInfoList.forEach(async (info) => {
        const res = await lectureItemService.postLectureItem({
          lectureId,
          lectureItemOrder: inputHead.lcItemOrder + forEachCount,
          lectureItemType: utils.convertLcItemType(inputHead.lcItemType),
        });
        let lcItemOffset = 0;
        if (inputHead.lcItemOffset !== null) {
          lcItemOffset = (3600 * inputHead.lcItemOffset.getHours()) +
            (60 * inputHead.lcItemOffset.getMinutes()) + inputHead.lcItemOffset.getSeconds();
        }
        const lectureItemId = res.data.lecture_item_id;
        await lectureItemService.putLectureItem({
          lectureItemId,
          name: inputHead.lcItemName,
          sequence: inputHead.lcItemSequence,
          result: inputHead.lcItemResult,
          offset: lcItemOffset,
          limit_length: inputHead.lcItemLimitLength,
        });
        await this.postChildLectureItem({
          lcItemId: lectureItemId,
          info,
          inputTail,
        });
        forEachCount += 1;
        if (forEachCount === inputTail.groupDataList.groupInfoList.length) {
          resolve(true);
        }
      });
    });
    await postLcItemFunc();
  }

  static async postChildLectureItem({ lcItemId, info, inputTail }) {
    await individualQuestionService.postIndividualQuestionInfo({
      lectureItemId: lcItemId,
    });
    this.putChildLectureItem({ lcItemId, info, inputTail });
  }

  static async putChildLectureItem({ lcItemId, info, inputTail }) {
    const type = (typeof info.type) === 'string' ? utils.convertQuestionType(info.type) : info.type;
    await individualQuestionService.putIndividualQuestionInfo({
      type,
      difficulty: info.difficulty.split('~').join(','),
      keyword: info.keywords,
      lectureItemId: lcItemId,
    });

     // 새로운 문항 score 변경 용도(bankLectureItemId 를 새로 생성된 lectureItemId로 변환)
    const oldIdList = [];
    inputTail.groupDataList.groupQuestionList.forEach(async (question) => {
      if (question.bank_lecture_item_id === undefined) { // 기존에 있던 아이템인 경우
        // 기존에 있던 아이템중 저장될 아이템 리스트(기존에 있던 개인화 문항 아이템 중
        // 여기에 속하지 않는 아이템(새로 추가 제외)은 삭제된다)
        oldIdList.push(question.data.lecture_item.lecture_item_id);
        // 점수 변경 용도
        await individualQuestionService.putIndividualQuestion({
          score: Number(question.score),
          lectureItemId: question.data.lecture_item.lecture_item_id,
        });
      } else if (question.group === info.group) { // 새로 추가된 아이템의 경우
        const indivRes = await individualQuestionService.postIndividualQuestion({ // 추가하고
          lectureItemId: lcItemId,
          lectureId: inputTail.lectureId,
          bankItemId: String(question.bank_lecture_item_id),
        });
        // 점수 변경 용도
        await individualQuestionService.putIndividualQuestion({
          score: Number(question.score),
          lectureItemId: indivRes.data.data[0].newActiveQuestion.lecture_item_id,
        });
      }
    });

    const res1 = await lectureService.getLecture({
      lectureId: inputTail.lectureId,
    });
    // 삭제된 아이템은 삭제하려고 분류

    const deleteItemList = res1.data.lecture_items.filter((item) => {
      if (item.type === 7 && item.questions[0].info_item_id === lcItemId
        && oldIdList.includes(item.lecture_item_id) === false) {
        return true;
      }
      return false;
    });
    // 삭제
    deleteItemList.forEach(async (item) => {
      await individualQuestionService
      .deleteIndividualQuestion({ lectureItemId: item.lecture_item_id });
    });
  }

  static async putLcItem({ inputHead, lectureItemId, inputTail }) {
    let lcItemOffset = 0;
    if (inputHead.lcItemOffset !== null) {
      lcItemOffset = (3600 * inputHead.lcItemOffset.getHours()) +
        (60 * inputHead.lcItemOffset.getMinutes()) + inputHead.lcItemOffset.getSeconds();
    }

    await lectureItemService.putLectureItem({
      lectureItemId,
      name: inputHead.lcItemName,
      order: inputHead.lcItemOrder,
      result: inputHead.lcItemResult,
      offset: lcItemOffset,
      limit_length: inputHead.lcItemLimitLength,
    });

    const info = inputTail.groupDataList.groupInfoList[0];
    this.putChildLectureItem({ lcItemId: lectureItemId, info, inputTail });
  }
}
