import http from './http';

export default {
  postLectureChating({
    lecture_id,
    content,
  }) {
    return http.post('/lecture_chatings', {
      lectureId: lecture_id,
      content,
    });
  },
  getLectureChating({
    lecture_id,
  }) {
    return http.get(`/lecture_chatings/${lecture_id}`);
  },
  deleteLectureChating({
    lecture_id,
  }) {
    return http.delete(`/lecture_chatings/${lecture_id}`);
  },
};
