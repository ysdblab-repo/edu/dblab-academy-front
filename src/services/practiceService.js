import http from './http';

export default {
  postPractice({
                 lectureItemId,
               }) {
    return http.post('/lecture_code_practices', {
      lectureItemId,
    });
  },
  putPractice({
                practiceId,
                code,
              }) {
    return http.put(`/lecture_code_practices/${practiceId}`, {
      code,
    });
  },
  getPracticeKeywords({
    practiceId,
  }) {
    return http.get(`/lecture_code_practices/${practiceId}/keywords`);
  },
  postPracticeKeywords({
    practiceId,
    data,
  }) {
    return http.post(`/lecture_code_practices/${practiceId}/keywords`, {
      data,
    });
  },
  deletePracticeKeywords({
    practiceId,
  }) {
    return http.delete(`/lecture_code_practices/${practiceId}/keywords`);
  },
  // 주피터 학생 코드 가져오기
  getCode({
    email_id, item_id, class_id,
  }) {
    return http.post('/lecture_code_practices/getStudent', {
      email_id,
      item_id,
      class_id,
    });
  },
};
