import deepCopy from 'deep-copy';
import isArray from 'lodash.isarray';
// import utils from '../utils';
import classService from '../services/classService';
import openspaceService from '../services/openspaceService';

export default {
  namespaced: true,
  state: {
    // //////////////////////////절취선////////////////////////// //
    /**
     * 공통 변수들
     * 초기값으로 []를 주니까 서버에서 받아온건데 비어있는건지
     * 아니면 아직 서버로부터 안받아온건지 구분이 안가서 null로 초기화함
     */
    curLectureId: null,
    openedClassList: null,
    goingClassList: null,
    finishedClassList: null,
    studyingClassList: null,
    teachingClassList: null,
    popularClassList: null,
    previewClassList: null,
    openPreviewClassList: null,
    // //////////////////////////절취선////////////////////////// //
  },
  getters: {
    isTeachingClassListEmpty(state) {
      if (!isArray(state.teachingClassList)) {
        return false;
      }
      return state.teachingClassList.length === 0;
    },
    isStudyingClassListEmpty(state) {
      if (!isArray(state.studyingClassList)) {
        return false;
      }
      return state.studyingClassList.length === 0;
    },
    currentTeachingClass: state => (classId) => {
      if (!Array.isArray(state.teachingClassList)) {
        return null;
      }
      return state.teachingClassList.find(item => item.class_id === classId);
    },
    currentStudyingClass: state => (classId) => {
      if (!Array.isArray(state.studyingClassList)) {
        return null;
      }
      return state.studyingClassList.find(item => item.class_id === classId);
    },
    currentPreviewClass: state => (classId) => {
      if (!Array.isArray(state.previewClassList)) {
        return null;
      }
      return state.previewClassList.find(item => item.class_id === classId);
    },
    isPreviewClassListEmpty(state) {
      if (!isArray(state.previewClassList)) {
        return false;
      }
      return state.previewClassList.length === 0;
    },
    currentOpenPreviewClass: state => (classId) => {
      if (!Array.isArray(state.openPreviewClassList)) {
        return null;
      }
      return state.openPreviewClassList.find(item => item.class_id === classId);
    },
  },
  mutations: {
    updateOpenedClassList(state, { openedClassList }) {
      state.openedClassList = openedClassList;
    },
    updateGoingClassList(state, { goingClassList }) {
      state.goingClassList = goingClassList;
    },
    updateFinishedClassList(state, { finishedClassList }) {
      state.finishedClassList = finishedClassList;
    },
    updateStudyingClassList(state, { studyingClassList }) {
      state.studyingClassList = studyingClassList;
    },
    deleteStudyingClass(state, { studyingClassIndex }) {
      state.studyingClassList.splice(studyingClassIndex, 1);
    },
    updateTeachingClassList(state, { teachingClassList }) {
      state.teachingClassList = teachingClassList;
    },
    deleteTeachingClass(state, { teachingClassIndex }) {
      state.teachingClassList.splice(teachingClassIndex, 1);
    },
    updatePopularClassList(state, { popularClassList }) {
      state.popularClassList = popularClassList;
    },
    updateCurLectureId(state, { lid }) {
      state.curLectureId = lid;
    },
    updatePreviewClassList(state, { previewClassList }) {
      state.previewClassList = previewClassList;
    },
    deletePreviewClass(state, { previewClassIndex }) {
      state.previewClassList.splice(previewClassIndex, 1);
    },
    updateOpenPreviewClassList(state, { openPreviewClassList }) {
      state.openPreviewClassList = openPreviewClassList;
    },
    deleteOpenPreviewClass(state, { openPreviewClassIndex }) {
      state.openPreviewClassList.splice(openPreviewClassIndex, 1);
    },
  },
  actions: {
    updateLectureId({ commit }, { lid }) {
      commit('updateCurLectureId', {
        lid,
      });
    },
    async putScore({ state }, { id, score }) {
      const res = await classService.putScore({
        id, score,
      });
      return res;
    },
    async getClassLists({ commit }) {
      const res = await classService.getClassLists();
      commit('updateOpenedClassList', {
        openedClassList: res.data.openedClasses,
      });
      commit('updateGoingClassList', {
        goingClassList: res.data.goingClasses,
      });
      commit('updateFinishedClassList', {
        finishedClassList: res.data.finishedClasses,
      });
    },
    async getOpenPreviewClass({ commit }, { affiliationId }) {
      const res = await openspaceService.getInstitutionList({
        affiliationId,
      });

      const tmp = res.data.openInstitutionClasses;
      const opc = [];
      
      tmp.forEach((x) => {
        opc.push(x.class);
      });

      commit('updateOpenPreviewClassList', {
        openPreviewClassList: opc,
      });
    },
    async getClass2({ state, commit }, { type, classId }) {
      const res = await classService.getClass({
        id: classId,
      });

      const newOpenPreviewClassList = deepCopy(state.openPreviewClassList);
      const currentClass = newOpenPreviewClassList.find(item => item.class_id === classId);

      currentClass.lectures = res.data.lectures;
      commit('updateOpenPreviewClassList', {
        openPreviewClassList: newOpenPreviewClassList,
      });
    },
    async getMyClassLists({ commit }) {
      const res = await classService.getMyClassList();
      const tmp = res.data.studyingClasses;
      const tmp2 = res.data.previewClasses;
      const sc = [];
      const uc = [];

      for (const x of tmp) {
        x.rate = await classService.getClassRating({classId: x.class_id,});
        x.rate = x.rate.data.rate;
				if (x.rate === null) {
    			x.rate = 0;
    		}
        // if (x.opened !== 1) { // 비공개가 아닌 공개, 마감 강의
        if (x.req_create_classes.length !== 0) { // 과목의 소속기관 유무 판단
          x.affiliation_name = x.req_create_classes[0].affiliation_description.description;
        } else {
          x.affiliation_name = '';
        }
        sc.push(x);
      }
      /* forEach await 문제 발생으로 for로 변경
      tmp.forEach(async (x) => {
        x.rate = await classService.getClassRating({classId: x.class_id,});
        x.rate = x.rate.data.rate;
				if (x.rate === null) {
    			x.rate = 0;
    		}
        // if (x.opened !== 1) { // 비공개가 아닌 공개, 마감 강의
        if (x.req_create_classes.length !== 0) { // 과목의 소속기관 유무 판단
          x.affiliation_name = x.req_create_classes[0].affiliation_description.description;
        } else {
          x.affiliation_name = '';
        }
        sc.push(x);
        console.log(x)
        // }
      }); */

      for (const x of tmp2) {
        x.rate = await classService.getClassRating({classId: x.class_id,});
				x.rate = x.rate.data.rate;
				if(x.rate === null) {
    			x.rate = 0;
        }
        if (x.req_create_classes.length !== 0) { // 과목의 소속기관 유무 판단
          if (x.req_create_classes[0].affiliation_description.isOpen === 0) {
            x.affiliation_name = x.req_create_classes[0].affiliation_description.description;
            uc.push(x);
          }
        } else {
          x.affiliation_name = '';
          uc.push(x);
        }
      }

      /* 위와 동일 사유
      tmp2.forEach(async (x) => {
        x.rate = await classService.getClassRating({classId: x.class_id,});
				x.rate = x.rate.data.rate;
				if(x.rate === null) {
    			x.rate = 0;
    		}
        if (x.req_create_classes.length !== 0) { // 과목의 소속기관 유무 판단
          if (x.req_create_classes[0].affiliation_description.isOpen !== 0) {
            return
          }
          x.affiliation_name = x.req_create_classes[0].affiliation_description.description;
        } else {
          x.affiliation_name = '';
        }
        uc.push(x);
      });*/

      commit('updateStudyingClassList', {
        studyingClassList: sc,
      });

      commit('updatePreviewClassList', {
        previewClassList: uc,
      });

      const tc = res.data.teachingClasses;
      
      for (const x of tc) {
        x.rate = await classService.getClassRating({classId: x.class_id,});
        x.rate = x.rate.data.rate;
				if(x.rate === null) {
    			x.rate = 0;
        }
        if (x.req_create_classes.length !== 0) { // 과목의 소속기관 유무 판단
          x.affiliation_name = x.req_create_classes[0].affiliation_description.description;
        } else {
          x.affiliation_name = '';
        }
      }
      /* 사유 동일
      tc.forEach(async (x) => {
        x.rate = await classService.getClassRating({classId: x.class_id,});
				x.rate = x.rate.data.rate;
				if(x.rate === null) {
    			x.rate = 0;
    		}
        if (x.req_create_classes.length !== 0) { // 과목의 소속기관 유무 판단
          x.affiliation_name = x.req_create_classes[0].affiliation_description.description;
        } else {
          x.affiliation_name = '';
        }
      }); */

      commit('updateTeachingClassList', {
        teachingClassList: tc,
      });
    },
    async getClass({ state, commit }, { type, classId }) {
      const res = await classService.getClass({
        id: classId,
      });
      if (type === 'TEACHER') {
        const newTeachingClassList = deepCopy(state.teachingClassList);
        const currentClass = newTeachingClassList.find(item => item.class_id === classId);
        currentClass.lectures = res.data.lectures;
        commit('updateTeachingClassList', {
          teachingClassList: newTeachingClassList,
        });
      } else {
        if (parseInt(res.data.type, 10) !== 4) {
          const newStudyingClassList = deepCopy(state.studyingClassList);
          const currentClass = newStudyingClassList.find(item => item.class_id === classId);
          currentClass.lectures = res.data.lectures;
          commit('updateStudyingClassList', {
            studyingClassList: newStudyingClassList,
          });
        } else if (parseInt(res.data.type, 10) === 4) {
          const newPreviewClassList = deepCopy(state.previewClassList);
          const currentClass = newPreviewClassList.find(item => item.class_id === classId);
          currentClass.lectures = res.data.lectures;
          commit('updatePreviewClassList', {
            previewClassList: newPreviewClassList,
          });
        }
      }
    },
    async getPopularClassList({ commit }) {
      const res = await classService.getMainClassLists();
      const mainClass = res.data.main_classes;
      for (let i = 0; i < mainClass.length; i += 1) {
        mainClass[i].classId = mainClass[i].class_id;
        mainClass[i].title = mainClass[i].name;
        mainClass[i]['teacher-list'] = [mainClass[i].master_name];
        const a = new Date(mainClass[i].start_time);
        const b = new Date(mainClass[i].end_time);
        mainClass[i].startDateStr = a.toLocaleDateString();
        mainClass[i].endDateStr = b.toLocaleDateString();
      }
      commit('updatePopularClassList', {
        popularClassList: mainClass,
      });
    },
    async postClassUser(_, { classId }) {
      await classService.postClassUser({
        id: classId,
      });
    },
    async deleteClassUser(_, { classId, userId }) {
      await classService.deleteClassUser({
        classId,
        userId,
      });
    },
  },
};
