// import axios from 'axios';
// import config from './config';
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
import http from './http';

export default {
  postRecord({
    video,
    comment
  }) {
    return http.post(`/record`, { video, comment });
  },
  getAllRecordList() {
    return http.get(`/record/all`);
  },
  getRecordVideo({
    video_id
  }) {
    return http.get(`/record/${video_id}`);
  },
  deleteRecordVideo({
    video_id
  }) {
    return http.delete(`/record/${video_id}`);
  }
};
