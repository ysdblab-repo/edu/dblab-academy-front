// import getLocale from 'browser-locale';
import jwtDecode from 'jwt-decode';
import moment from 'moment';
import isNil from 'lodash.isnil';
import isBoolean from 'lodash.isboolean';
import isNumber from 'lodash.isnumber';
import browser from 'browser-detect';
// eslint-disable-next-line
const re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

export default {
  checkBrowser() {
    const result = browser();
    return result;
  },
  async importScript(src, callback) {
    (function() {
      function async_load(){
        let x = document.getElementsByTagName('script')[0];
        let cnt = src.length;
        for (let srcData of src) {
          let s = document.createElement('script');
          s.type = 'text/javascript';
          s.async = true;
          s.src = srcData;
          s.onload = () => {
            if (cnt === 1) {
              callback();
            } else {
              cnt -= 1;
            }
          }
          x.parentNode.insertBefore(s, x);
        }
      }
      async_load();
      // window.attachEvent ? window.attachEvent('onload', async_load) : window.addEventListener('load', async_load, false);
    })();
  },
  getJwtFromLocalStorage() {
    const jwt = localStorage.getItem('jwt') || '';
    // TODO: Check expiration date periodically
    // TODO: Check expiration date left > 24hrs or not

    // if jwt is expired, erase it then return empty string
    if (this.isJwtExpired(jwt)) {
      localStorage.setItem('jwt', '');
      return '';
    }
    return jwt;
  },
  getDefaultLocale() {
    // TODO: 나중에 로케일 작업하면 밑에 코드로 바꾸기
    // const browserLocale = getLocale().split('-')[0];
    // return localStorage.getItem('locale') || browserLocale || 'ko';
    return 'ko';
  },
  isJwtExpired(jwt) {
    if (!jwt) {
      return true;
    }
    // const encodedPayload = jwt.split('.')[1];
    // console.log('encodedPayload', encodedPayload);
    // const payload = JSON.parse(atob(encodedPayload));
    // console.log('payload', payload);
    let isExpired;
    try {
      const payload = jwtDecode(jwt);
      isExpired = Date.now() > payload.exp * 1000;
    } catch (error) {
      isExpired = true;
    }
    return isExpired;
  },
  getAuthTypeFromJwt(jwt) {
    if (jwt.length === 0) {
      return null;
    }
    let decodedJwt;

    try {
      decodedJwt = jwtDecode(jwt);
    } catch (error) {
      console.error(error.toString); // eslint-disable-line no-console
    }

    return decodedJwt ? decodedJwt.authType : null;
  },
  getAuthTypeFromJwt2() {
    const jwt = this.getJwtFromLocalStorage();
    if (jwt.length === 0) {
      return null;
    }
    let decodedJwt;

    try {
      decodedJwt = jwtDecode(jwt);
    } catch (error) {
      console.error(error.toString); // eslint-disable-line no-console
    }
    return decodedJwt ? decodedJwt.authType : null;
  },
  getEmailFromJwt() {
    const jwt = this.getJwtFromLocalStorage();
    if (jwt.length === 0) {
      return null;
    }
    let email;
    try {
      email = jwtDecode(jwt).email_id;
    } catch (error) {
      email = null;
    }
    return email;
  },
  getNameFromJwt() {
    const jwt = this.getJwtFromLocalStorage();
    if (jwt.length === 0) {
      return null;
    }
    let name;
    try {
      name = jwtDecode(jwt).name;
    } catch (error) {
      name = null;
    }
    return name;
  },
  getUserIdFromJwt() {
    const jwt = this.getJwtFromLocalStorage();
    if (jwt.length === 0) {
      return null;
    }
    // TODO: try cath jwtDecode
    return jwtDecode(jwt).authId;
  },
  formatDate(d) {
    // console.log('formatDate', d, d.toLocaleDateString('en-US').split('-'));
    // eslint-disable-next-line
    return moment(d).format('YYYY-MM-DD HH:mm:ss');
  },
  formatSec2Time(secs, format) {
    let hr = Math.floor(secs / 3600);
    let min = Math.floor((secs - (hr * 3600)) / 60);
    let sec = Math.floor(secs - (hr * 3600) - (min * 60));

    if (hr < 10) {
      hr = `0${hr}`;
    }
    if (min < 10) {
      min = `0${min}`;
    }
    if (sec < 10) {
      sec = `0${sec}`;
    }
    if (hr) {
      hr = '00';
    }

    if (format != null) {
      let formattedTime = format.replace('hh', hr);
      formattedTime = formattedTime.replace('mm', min);
      formattedTime = formattedTime.replace('ss', sec);
      return formattedTime;
    }
    return `${hr}:${min}:${sec}`;
  },
  convertLcType(type) {
    const mapping = ['[유인]', '[무인]단체', '[무인]개인', '[무인]개인', '[소그룹방]', '[회의방]'];
    if (typeof type === 'number') {
      return mapping[type];
    } else if (typeof type === 'string') {
      return mapping.indexOf(type);
    }
    return new Error(`not defined scType ${type}`);
  },
  convertScItemType(scItemType) {
    const mapping = ['문항', '설문', '세션자료', '숙제', '실습', '토론', '개인화 문항', '개인화 문항 구성 요소'];
    if (typeof scItemType === 'number') {
      return mapping[scItemType];
    } else if (typeof scItemType === 'string') {
      return mapping.indexOf(scItemType);
    }
    return new Error(`not defined scItemType ${scItemType}`);
  },
  convertLcItemTypeToNumber(lcItemType) {
    const mapping = ['question', '문항', 'survey', '설문', 'practice', '실습', 'discussion', '토론', 'note', '자료', 'coding', '코딩 과제', 'individualQuestion', '개인화 문항', 'individualQuestion sources', '개인화 문항 구성 요소'];
    if (typeof lcItemType === 'number') {
      return lcItemType;
    } else if (typeof lcItemType === 'string') {
      return Math.floor(mapping.indexOf(lcItemType) / 2);
    }
    return new Error(`not defined lcItemType ${lcItemType}`);
  },
  convertLcItemType(lcItemType) {
    const mapping = ['question', 'survey', 'practice', 'discussion', 'note', 'coding', 'individualQuestion', 'individualQuestion sources'];
    if (typeof lcItemType === 'number') {
      return mapping[lcItemType];
    } else if (typeof lcItemType === 'string') {
      return mapping.indexOf(lcItemType);
    }
    return new Error(`not defined lcItemType ${lcItemType}`);
  },
  convertLcItemTypeKor(lcItemType) {
    return ['문항', '설문', '실습', '토론', '자료', '코딩 과제', '개인화 문항', '개인화 문항 구성 요소'][lcItemType];
  },
  // 시즌2용 유틸함수
  convertQuestionType2(questionType) {
    const mapping = [
      'MULTIPLE_CHOICE',
      'SHORT_ANSWER',
      'DESCRIPTION',
      'SW',
      'SQL',
      'VIDEO',
      'MULTIMEDIA',
      'CONNECTION',
    ];
    if (typeof questionType === 'number') {
      return mapping[questionType];
    } else if (typeof questionType === 'string') {
      return mapping.indexOf(questionType);
    }
    return new Error(`not defined questionType ${questionType}`);
  },
  // 시즌2용 유틸함수
  convertSurveyType2(surveyType) {
    const mapping = [
      'MULTIPLE_CHOICE',
      'DESCRIPTION',
    ];
    if (typeof surveyType === 'number') {
      return mapping[surveyType];
    } else if (typeof surveyType === 'string') {
      return mapping.indexOf(surveyType);
    }
    return new Error(`not defined surveyType ${surveyType}`);
  },
  convertNoteType(noteType) {
    const mapping = [
      'IMAGE',
      'DOCS',
      'LINK',
      'YOUTUBE',
    ];
    if (typeof noteType === 'number') {
      return mapping[noteType];
    } else if (typeof noteType === 'string') {
      return mapping.indexOf(noteType);
    }
    return new Error(`not defined noteType ${noteType}`);
  },
  convertQuestionType(questionType) {
    const mapping = ['객관', '단답', '서술', 'SW', 'SQL', '비디오', '멀티미디어', '연결형'];
    if (typeof questionType === 'number') {
      return mapping[questionType];
    } else if (typeof questionType === 'string') {
      return mapping.indexOf(questionType);
    }
    return new Error(`not defined questionType ${questionType}`);
  },
  // convertScItemOrder(scItemOrder) {
  //   const mapping = ['예습', '본강의', '복습'];
  //   if (typeof scItemOrder === 'number') {
  //     return mapping[scItemOrder];
  //   } else if (typeof scItemOrder === 'string') {
  //     return mapping.indexOf(scItemOrder);
  //   }
  //   return new Error(`not defined scItemOrder ${scItemOrder}`);
  // },
  convertBoolean(b) {
    if (isBoolean(b)) {
      return b === true ? 1 : 0;
    } else if (isNumber(b)) {
      return b === 1;
    }
    return new Error(`not defined b ${b}`);
  },
  convertSecondTohhmmss(second) {
    let hours = Math.floor(second / 3600);
    let minutes = Math.floor((second - (hours * 3600)) / 60);
    let seconds = second - (hours * 3600) - (minutes * 60);
    if (hours < 10) hours = `0${hours}`;
    if (minutes < 10) minutes = `0${minutes}`;
    if (seconds < 10) seconds = `0${seconds}`;
    return `${hours}:${minutes}:${seconds}`;
  },
  isValidEmail(emailString) {
    return re.test(emailString);
  },
  assignIfNotNil(p, variable, newKey) {
    const originalKey = Object.keys(variable)[0];
    const key = newKey || originalKey;
    // console.log('assignIfNotNil', key, variable);
    if (!isNil(variable[originalKey])) {
      Object.assign(p, { [key]: variable[originalKey] });
    }
  },
  sortSc(sc, currentEditingScItemIndex) {
    const beforeSortedScItem = sc[currentEditingScItemIndex];
    let tempMap = new Map();
    sc.forEach((item, index) => {
      tempMap.set(item, index);
    });
    // for (let i = 0; i < sc.length; i += 1) { window.console.log('before sort', sc[i].id); }
    const activeSortedSc = sc.sort((a, b) => {
      const diff = a.activeStartOffsetSec - b.activeStartOffsetSec;
      if (diff !== 0) { return diff; }
      return tempMap.get(a) - tempMap.get(b);
    });
    // for (let i = 0; i < sc.length; i += 1) { window.console.log('active sort', sc[i].id); }

    tempMap = new Map();
    activeSortedSc.forEach((item, index) => {
      tempMap.set(item, index);
    });
    const orderSortedSc = activeSortedSc.sort((a, b) => {
      const diff = a.order - b.order;
      if (diff !== 0) { return diff; }
      return tempMap.get(a) - tempMap.get(b);
    });
    // for (let i = 0; i < sc.length; i += 1) { window.console.log('order sort', sc[i].id); }

    const changedCurrentEditingScItemIndex = sc.map(x => x.id).indexOf(beforeSortedScItem.id);
    return { orderSortedSc, changedCurrentEditingScItemIndex };
  },
  downloadFile(baseUrl, file) {
    const link = document.createElement('a');
    link.href = baseUrl + file.client_path;
    link.download = file.name;
    link.target = '_blank';

    document.body.appendChild(link);
    link.click();
    window.setTimeout(() => {
      document.body.removeChild(link);
    }, 1000);
  },
  downloadFiles(baseUrl, filelist) {
    function downloadNext(i) {
      if (i >= filelist.length) {
        return;
      }

      const link = document.createElement('a');
      link.href = baseUrl + filelist[i].client_path;
      link.download = filelist[i].name;
      link.target = '_blank';
  
      document.body.appendChild(link);
      link.click();
      window.setTimeout(() => {
        document.body.removeChild(link);
      }, 1000);
      
      window.setTimeout(() => {
        downloadNext(i + 1);
      }, 500);
    }
    downloadNext(0);
  },
  getWindowSize() {
    return {
      width: Math.max(document.documentElement.clientWidth, window.innerWidth || 0),
      height: Math.max(document.documentElement.clientHeight, window.innerHeight || 0),
    };
  },
  getViewportSize() {
    const width = this.getWindowSize().width;
    if (width >= 1200) return 'lg';
    if (width >= 992) return 'md';
    if (width >= 768) return 'sm';
    return 'xs';
  },
  snakeCase(string) {
    return string.replace(/\W+/g, " ")
      .split(/ |\B(?=[A-Z])/)
      .map(word => word.toLowerCase())
      .join('_');
  },
};
