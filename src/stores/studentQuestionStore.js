// import utils from '../utils';
import studentService from '../services/studentService';

export default {
  namespaced: true,
  state: {
    lectureHomework: null,  // 해당 강의 숙제여부 체크
    lectureId: 0,
    mode: 0, // 0 - list, 1 - new, 2 - modify
    post: null,
    put: null,
    index: -1,
    message: null,                  // 예외처리 메시지
    studentQuestionList: [],        // 내가 작성한 문항 리스트
    studentEstimateQuestionList: [], // 평가해야할 문항리스트
    studentQuestionKeywords: [], // 디테일 문항 키워드 리스트
    dialogVisible: false,           // 문항평가하기 컴포넌트 다이얼로그 속성값
    studentEstimateCheck: null,
    modifyQuestion: { student_question_keywords: [] },
    newQuestion: {
      name: null,
      question: null,
      score: 0,
      difficulty: 0,
      choice: [],
      answer: [],
      isCheck: false,
    },
  },
  mutations: {
    updateStudentQuestionList(state, { studentQuestionList }) {
      state.studentQuestionList = studentQuestionList;
    },
    pushStudentQuestion(state, { studentQuestion }) {
      state.studentQuestionList.push(studentQuestion);
    },
    deleteStudentQuestion(state, { index }) {
      state.studentQuestionList.splice(index, 1);
    },
    transferStudentQuestion(state, { index }) {
      state.modifyQuestion = state.studentQuestionList[index];
      if (state.modifyQuestion.type === '객관') {
        for (let i = 0; i < state.studentQuestionList[index].answer.length; i++) {
          state.studentQuestionList[index].answer[i] = state.studentQuestionList[index].answer[i] * 1;
        }
      }
      if (state.studentQuestionList[index].answer === null) { return; }
      /*for (let i = 0; i < state.modifyQuestion.answer.length; i += 1) {
        state.modifyQuestion.answer[i] = parseInt(state.modifyQuestion.answer[i], 10);
      }*/
    },
    updateStudentQuestionIndex(state, index) {
      state.index = index;
    },
    updateStudentQuestionMode(state, { mode }) {
      state.mode = mode;
    },
    updateStudentEstimateQuestionList(state, { studentEstimateQuestionList }) {
      state.studentEstimateQuestionList = studentEstimateQuestionList;
    },
    updateLectureId(state, { lid }) {
      state.lectureId = lid;
    },
    updateLectureHomework(state, { lectureHomework }) {
      state.lectureHomework = lectureHomework;
    },
    updateStudentQuestionKeywords(state, { studentQuestionKeywords }) {
      state.studentQuestionKeywords = studentQuestionKeywords;
    },
    updateDialogVisible(state, { value }) {
      state.dialogVisible = value;
    },
    updateStateStudentQuestion(state, { idx }) {
      state.studentEstimateQuestionList[idx].estimated = '완료';
      // alert(`updated state - ${JSON.stringify(state.studentEstimateQuestionList)}`);
    },
    updateMessage(state, { message }) {
      state.message = message;
    },
  },
  getters: {
    getIndex: function (state) { // eslint-disable-line
      return state.index;
    },
  },
  actions: {
    transferStudentQuestion({ commit }, { index }) {
      commit('transferStudentQuestion', { index });
    },
    updateStudentQuestionMode1({ commit, dispatch, state }, { mode }) {
      if (mode === 0) {
        dispatch('getQuestionList', { lectureId: state.lectureId });
      }
      commit('updateStudentQuestionMode', { mode });
    },
    pushQuestion({ commit }, { data }) {
      const q = data.data;
      const date = new Date(q.createdAt);
      q.submitTime = date.toString().split(' GMT')[0];
      if (q.type === 0) {
        q.type = '객관';
      } else if (q.type === 1) {
        q.type = '단답';
      } else if (q.type === 2) {
        q.type = '파일';
      }
      commit('pushStudentQuestion', { studentQuestion: q });
    },
    async getQuestionList({ commit }, { lectureId }) {
      const results = [];
      const questions = await studentService.getQuestionList({ id: lectureId });
      for (let i = 0; i < questions.data.length; i += 1) {
        results.push(questions.data[i]);
        const date = new Date(questions.data[i].createdAt);
        results[i].submitTime = date.toString().split(' GMT')[0];
        if (questions.data[i].type === 0) {
          results[i].type = '객관';
        } else if (questions.data[i].type === 1) {
          results[i].type = '단답';
        } else if (questions.data[i].type === 2) {
          results[i].type = '파일';
        }
      }
      // alert(JSON.stringify(questions.data));
      commit('updateStudentQuestionList', {
        studentQuestionList: results,
      });
    },
    async deleteQuestion({ state, commit }, { lectureId, index }) {
      const res = await studentService.deleteQuestion({
        id: lectureId,
        qId: state.studentQuestionList[index].student_question_id,
      });
      if (res) {
        commit('deleteStudentQuestion', { index });
      }
      // commit('')
    },
    async putQuestion({ state, commit }, { data }) {
      // alert(JSON.stringify(data));
      const r = await studentService.putQuestion({
        id: data.lecture_id,
        qId: data.student_question_id,
        name: data.name,
        question: data.question,
        score: data.score,
        difficulty: data.difficulty,
        choice: data.choice,
        answer: data.answer,
      });
      return r;
    },
    async postQuestion({ state }) {
      // console.log('state', state);
      // if (!getters.isNewClassValid) {
      //   throw new Error('invalid');
      // }
      // TODO: pass intendedLectureNum
      // if (getters.)
      // if (state.name === null) {

      // }
      // if (state.choice.length === 0) {

      // }
      // if (state.answer.length === 0) {

      // }
      await studentService.postQuestion(state.newQuestion);
    },
    async deleteKeyword({ state }, { id, qId }) {
      const res = await studentService.deleteKeyword({ id, qId });
      return res;
    },
    async postKeyword({ state }, { id, qId, data }) {
      const res = await studentService.postKeyword({ id, qId, data });
      if (res) {
        // alert(JSON.stringify(res));
      } else {
        // alert('post keywords failure');
      }
      return res;
      // await studentService.postKeyword({ id, qId, data });
    },
    async getEstimateQuestion({ commit }, { id }) {
      const results = await studentService.getEstimateQuestion({ id });
      commit('updateStudentEstimateQuestionList', { studentEstimateQuestionList: results });
    },
    async getLectureHomeworkCheck({ commit }, { id }) {
      const result = await studentService.getLectureHomeworkCheck({ id });
      commit('updateLectureHomework', { lectureHomework: result.data });
    },
    async getSudentEstimateQuestionList({ commit }, { id }) {
      const result = await studentService.getStudentEstimateQuestionList({ id });
      if (!result.data.success) {
        commit('updateMessage', { message: result.data.message });
        return;
      }
      const estimates = [];
      for (let i = 0; i < result.data.result.length; i += 1) {
        estimates.push(result.data.result[i]);
        if (result.data.result[i].type === 0) {
          estimates[i].type = '객관';
        } else if (result.data.result[i].type === 1) {
          estimates[i].type = '단답';
        } else {
          estimates[i].type = '파일';
        }
        estimates[i].estimated = '미완료';
      }
      // alert(`estimate - ${JSON.stringify(estimates)}`);

      commit('updateStudentEstimateQuestionList', { studentEstimateQuestionList: estimates });
    },
    async getStudentQuestionKeywords({ commit }, { id, qId }) {
      const result = await studentService.getStudentQuestionKeywords({ id, qId });
      commit('updateStudentQuestionKeywords', { studentQuestionKeywords: result.data });
    },
    async postStudentQuestionScore({ state, commit }, { id, qId, score, comment }) {
      const result = await studentService.postStudentQuestionScore({ id, qId, score, comment });
      // alert(`result - ${JSON.stringify(result.data)}`);
      if (result.data.success) {
        for (let i = 0; i < state.studentEstimateQuestionList.length; i += 1) {
          if (state.studentEstimateQuestionList[i].student_question_id === qId) {
            // state.studentEstimateQuestionList[i]['estimated'] = true;
            commit('updateStateStudentQuestion', { idx: i });
          }
        }
      }
    },
  },
};
