import _ from 'lodash';
import attendService from '../services/attendService';
import userService from '../services/userService';

export default {
  namespaced: true,
  state: {
    curState: 0,
    curStep: 0,
    curLectureId: null,
		lectureList: [],
    curLectureItems: [],  // question list
    curLectureItems2: [], // survey list
    lectureResult: [],
    studentResult: [],
    studentReportResult: [],
    lectureTotalReportResult: [],
    studentTotalReportResult: null,
    studentReportStd: null,
    lectureReportStd: null,
  },
  mutations: {
    updateCurLectureId(state, { lectureId }) {
      state.curLectureId = lectureId;
    },
    updateLectureList(state, { result }) {
      state.lectureList = result;
		},
    updateLectureItemList(state, { result }) {
			state.curLectureItems.splice(0, state.curLectureItems.length);
      state.curLectureItems = result;
    },
    updateLectureItemList2(state, { result }) {
      state.curLectureItems2.splice(0, state.curLectureItems2.length);
      state.curLectureItems2 = result;
    },
    updateCurStep(state, { index }) {
      state.curStep = index;
    },
    updateCurState(state, { index }) {
      state.curState = index;
    },
    updateStudentList(state, { result }) {
      state.studentResult = result;
    },
    updateStudentReportResult(state, { result }) {
      state.studentReportResult = result;
    },
    updateLectureTotalReportResult(state, { result }) {
      state.lectureTotalReportResult = result;
    },
    updateStudentTotalReportResult(state, { result }) {
      state.studentTotalReportResult = result;
    },
    updateLectureReportStd(state, { result }) {
      state.lectureReportStd = result;
    },
    updateStudentReportStd(state, { result }) {
      state.studentReportStd = result;
    },
  },
  actions: {
    async getLectureList({ commit }, { classId }) {
			const result = await attendService.getLectureList({ classId });
			commit('updateLectureList', { result: result.data });
    },
    async getLectureAuthenticationList({ commit }, { lectureId, classId }) {
			const teacher_result = await attendService.getLectureAuths({ lectureId });
			commit('updateLectureItemList', { result: teacher_result.data });
      const student_result = await attendService.getLectureSTDAuths({ lectureId });
			const user_class = await userService.getUserClassesByClassId({ classId });
			let us_cl = [];
			user_class.data.user_classes.forEach(element => {
				us_cl.push(element.user_id);
			});
			let checked = [];
      let checkedin = student_result.data[0] ? [student_result.data[0].student_id] : [];
      let flag = student_result.data[0] ? student_result.data[0].auth_date: '';
			for(let i=0; i<student_result.data.length; i++) { // 출석한 사람만 표기
				if (flag !== student_result.data[i].auth_date ) {
					let obj = {};
					obj.date = flag;
					obj.check = checkedin;
					checked.push(obj);
					flag = student_result.data[i].auth_date;
					checkedin = [];
				}
				let student = {};
				student.index = i+1;
				student.std_name = student_result.data[i].name;
				student.email = student_result.data[i].email_id;
				student.attend_date = student_result.data[i].auth_date;
				if ( us_cl.includes(student_result.data[i].student_id) ) {
					student.attending = '출석';
					checkedin.push(student_result.data[i].student_id);
				}
				student_result.data[i] = student;
			}
			let us_name = [];
			let us_email = [];
			user_class.data.user_classes.forEach(element => {
				us_name.push(element.user.name);
				us_email.push(element.user.email_id);
			});
			for (let i=0; i<checked.length; i++) { // 출석하지 않은 사람 추가
				for (let j=0; j<us_cl.length; j++) {
					if ( !checked[i].check.includes(us_cl[j]) ) {
						let obj = {};
						obj.index = student_result.data.length;
						obj.std_name = us_name[j];
						obj.email = us_email[j];
						obj.attend_date = checked[i].date;
						obj.attending = '결석';
						student_result.data.push(obj);
					}
				}
			}
      commit('updateLectureItemList2', { result: student_result.data });
		},
    async postLectureResult({ commit }, { lectureId, result, type }) {
      const studentResult = await attendService.postLectureResult({ lectureId, result, type });
      commit('updateStudentList', { result: studentResult.data });
    },
    async getClassResult({ commit }, { classId, type }) {
      const totalResult = await attendService.getClassResult({ classId, type });
      // alert(JSON.stringify(totalResult.data));
      const lectureResult = totalResult.data.lectureResult;
      const studentResult = totalResult.data.studentResult;
      const userResult = totalResult.data.userResult;
      const istudent = _.groupBy(studentResult, 'student_id');

      const students = [];
      const lecturesAgg = {};
      const lectureName = {};
      const studentsAgg = {};
      for (let i = 0; i < lectureResult.length; i += 1) {
        lecturesAgg[lectureResult[i].lecture_id] = 0;
        lectureName[lectureResult[i].lecture_id] = lectureResult[i].name;
      }
      // console.log(`lecture init - ${JSON.stringify(lecturesAgg)}`);
      for (let i = 0; i < userResult.length; i += 1) {
        const items = istudent[userResult[i].user_id];
        const student = {
          name: userResult[i].name,
          student_id: userResult[i].user_id,
        };
        studentsAgg[userResult[i].user_id] = 0;
        for (let j = 0; j < lectureResult.length; j += 1) { // 임의로 초기화
          student[lectureResult[j].lecture_id] = 0;
        }
        if (items === undefined) {
          continue; // eslint-disable-line
        }
        for (let j = 0; j < items.length; j += 1) {
          student[items[j].lecture_id] += items[j].score;
          studentsAgg[userResult[i].user_id] += items[j].score;
          lecturesAgg[items[j].lecture_id] += items[j].score;
        }
        students.push(student);
      }
      const keys = Object.keys(lecturesAgg);
      const tmpResult = [];
      for (let i = 0; i < keys.length; i += 1) {
        const obj = { lecture_id: keys[i], sum: lecturesAgg[keys[i]], name: lectureName[keys[i]] };
        tmpResult.push(obj);
      }
      const lectureStd = {};
      for (let i = 0; i < keys.length; i += 1) {
        lectureStd[`${keys[i]}`] = 0;
      }
      for (let i = 0; i < keys.length; i += 1) {
        const lectureAvg = lecturesAgg[keys[i]] / students.length;
        let lectureDiffSum = 0;
        for (let j = 0; j < students.length; j += 1) {
          const sum = students[j][keys[i]];
          let diff = lectureAvg - sum;
          if (diff < 0) { diff = -diff; }
          lectureDiffSum += diff;
        }
        lectureStd[`${keys[i]}`] = parseFloat((lectureDiffSum / keys.length).toFixed(2));
      }
      // let tmp = Object.values(studentsAgg);
      // let studentSum = 0;
      // let studentAvg = 0;
      const studentStd = {};
      for (let i = 0; i < students.length; i += 1) {
        let diffSum1 = 0;
        const studentAvg = studentsAgg[students[i].student_id] / tmpResult.length;
        for (let j = 0; j < keys.length; j += 1) {
          let diff = studentAvg - students[i][keys[j]];
          if (diff < 0) { diff = -diff; }
          diffSum1 += diff;
        }
        studentStd[students[i].student_id] = parseFloat((diffSum1 / tmpResult.length).toFixed(2));
      }
      // console.log(`student std result - ${JSON.stringify(studentStd)}`);
      // console.log(`lecture std result - ${JSON.stringify(lectureStd)}`);
      // alert(JSON.stringify(tmpResult));
      // alert(`student - ${JSON.stringify(students)}`);
      // alert(`student agg - ${JSON.stringify(studentsAgg)}`);
      // alert(`lecture agg - ${JSON.stringify(tmpResult)}`);
      commit('updateLectureReportStd', { result: lectureStd });
      commit('updateStudentReportStd', { result: studentStd });
      commit('updateStudentReportResult', { result: students });
      commit('updateStudentTotalReportResult', { result: studentsAgg });
      commit('updateLectureTotalReportResult', { result: tmpResult });
		},
  },
};
