import LcItemHandler from './index';
import codingService from '../../services/codingService';
// import utils from '../../utils';

export default class codingHandler extends LcItemHandler {

  /* eslint-disable no-param-reassign */
  static async initViewModel(vm) {
    const item = vm.lectureItem;
    const p = item.codings[0];

    const keywordList = await codingService.getCodingKeywords({
      codingId: p.coding_id,
    });
    keywordList.data.forEach((element) => {
      element.score = element.score_portion;
    });
    vm.inputTail.assignedKeywordList = keywordList.data;
    vm.inputTail.description = p.description;
  }
  /* eslint-enable no-param-reassign */

  // @Override
  static async postChildLectureItem({ lcItemId, inputTail }) {
    const res1 = await codingService.postCoding({
      lectureItemId: lcItemId,
    });
    const codingId = res1.data.coding_id;

    this.putChildLectureItem({
      codingId,
      inputTail,
    });
  }

  static async putChildLectureItem({ codingId, inputTail }) {
    await codingService.putCoding({
      codingId,
      description: inputTail.description,
    });
    await codingService.deleteCodingKeywords({
      codingId,
    });
    await codingService.postCodingKeywords({
      codingId,
      data: inputTail.assignedKeywordList,
    });
  }
}
