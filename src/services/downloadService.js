import http from './http';

// service 관련 documentation은 구글 스프레드 시트를 참조하는게 빠름!
export default {
  studentEvaluationDownload(lectureId) {
    return http.get(`/downloads/studentEvaluation/${lectureId}`)
  },
  studentResultDownload(lectureId, type) {
    return http.get(`/downloads/studentResult/${lectureId}/${type}`);
  }
};
