import http from './http';

export default {
  getCheatingSmallLecture({
    lectureId,
  }) {
    return http.get(`/cheating_small?lecture_id=${lectureId}`);
  },
  postCheatingSmall({
    lectureId,
    leaderFollowerCheatingArray,
  }) {
    return http.post('/cheating_small', {
      lecture_id: lectureId,
      leader_follower_cheating_array: leaderFollowerCheatingArray,
    });
  },
  getCheatingSmallClass({
    classId,
  }) {
    return http.get(`/cheating_small?class_id=${classId}`);
  },
  getCheatinLarge({
    lectureItemId,
  }) {
    return http.get(`/cheating_large?lecture_item_id=${lectureItemId}`);
  },
  postCheatingLarge({
    lectureItemId,
    answer,
    userIdArray,
  }) {
    return http.post('/cheating_large', {
      lecture_item_id: lectureItemId,
      answer,
      user_id_array: userIdArray,
    });
  },
};
