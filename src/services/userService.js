import http from './http';
import axios from 'axios'
import { baseUrl } from './config';

export default {
  searchUserByEmail({ email }) {
    return http.get(`/users/search?email=${email}`);
  },
  getUserBelongings() {
    return http.get('/users/belonging');
  },
  postUserBelonging({ code }) {
    return http.post('/users/belonging', {
      code
    });
  },
  postUserBelongingExcel({
    code,
    user_id,
  }) {
    return http.post('/users/belongingExcel', {
      code,
      user_id,
    });
  },
  getUserClassesByClassId({ classId }) {
    return http.get(`/user_classes/classId/${classId}/student`);
  },
  deleteUserClasses({ classId, userId }) {
    return http.delete(`/user_classes/classId/${classId}/userId/${userId}`);
  },
  createUserClassesDeletelog({ classId, userId }) {//choi-->
    return http.get(`/user_class_delete_logs/classId/${classId}/userId/${userId}`);
  },
  postUserProfile(form) {
    var api = `${baseUrl}/users/upload`;
    axios
    .post(api, form)
    .then(response => {
    })
    .catch(err => {
      console.log(err);
    });
  },
  getUserNameByUserId({
    userId,
  }) {
    return http.get(`/users/${userId}`);
  },
  searchUserByForm({ 
    email,
    name,
   }) {
    return http.get(`/users/check/${email}/${name}`);
  },
  postTeacherBelongingByAdmin({
    userId,
  }) {
    return http.post('/user_belonging/binding_teacher', {
      userId,
    });
  },

  getUserEventNotificationList({ userId }) {
    return http.get(`/users/event/notification/${userId}`);
    
  },
  // 2021-05-14 사용자 알림 리스트를 받아온다.
  // getUserEventNotificationList({
  //   userId,
  // }) {    
  //   return http.get(`/users/event/notification/${userId}`);
  // },

};
