import http from './http';

export default {
  getAttitude({
    classId,
  }) {
    return http.get(`/attitude?classId=${classId}`);
  },
  postAttiude({
    classId,
    userIdAttitudeArray,
  }) {
    return http.post('/attitude', {
      classId,
      userIdAttitudeArray,
    });
  },
};
