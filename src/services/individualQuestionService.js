import http from './http';
import utils from '../utils';

export default {
  postIndividualQuestionInfo({
    lectureItemId,
  }) {
    return http.post('/individuals', { lectureItemId });
  },
  putIndividualQuestionInfo({
    type,
    difficulty,
    keyword,
    lectureItemId,
  }) {
    const param = {};

    utils.assignIfNotNil(param, { type });
    utils.assignIfNotNil(param, { difficulty });
    utils.assignIfNotNil(param, { keyword });

    return http.put(`/individuals/${lectureItemId}`, param);
  },
  deleteIndividualQuestionInfo({
    lectureItemId,
  }) {
    return http.delete(`/individuals/${lectureItemId}`);
  },
  postIndividualQuestion({
    lectureItemId,
    lectureId,
    bankItemId,
  }) {
    return http.post('/individuals/questions', {
      lectureItemId,
      lectureId,
      bankItemId,
    });
  },
  putIndividualQuestion({
    lectureItemId,
    score,
  }) {
    return http.put(`/individuals/questions/${lectureItemId}`, {
      score,
    });
  },
  deleteIndividualQuestion({
    lectureItemId,
  }) {
    return http.delete(`/individuals/questions/${lectureItemId}`);
  },
  getIndividualQuestionIndice({
    lectureItemId,
  }) {
    return http.get(`/individuals/questions/indices/${lectureItemId}`);
  },
};
