/* eslint-disable import/prefer-default-export */
/* eslint-disable prefer-const */

const isProd = document.URL.includes('preswot');

let serverUrl;
let dockerUrl;
// let faceServerUrl;
let backPort;
let pingPort;
// let facePort;
let socketPort;
let frontUrl;

if (isProd) {
  frontUrl = 'https://preswot.com';
  serverUrl = 'https://wiseswot.com';
  dockerUrl = 'http://wiseswot.com';
  // faceServerUrl = 'http://54.180.160.115';
  backPort = 8010;
  socketPort = 8011;
  pingPort = 8012;
  // facePort = 8013;
} else { // 기본적으로 테스트 백엔드로 연결됩니다. 다른 백엔드를 원한다면 이곳을 수정하여 사용하세요.
  frontUrl = 'http://13.125.246.84:8080'; // 'http://13.125.246.84:8080';
  // serverUrl = 'http://preswottestback.duckdns.org';
  // dockerUrl = 'http://preswottestback.duckdns.org';
  serverUrl = 'http://localhost';
  dockerUrl = 'http://localhost';
  // serverUrl = 'https://wiseswot.com';
  // dockerUrl = 'http://wiseswot.com';
  // 연구실 서버
  // serverUrl = 'http://165.132.105.44';
  // dockerUrl = 'http://165.132.105.44';
  // faceServerUrl = 'http://54.180.160.115';
  backPort = 8010;
  socketPort = 8011;
  pingPort = 8012;
  // facePort = 8013;
}
/*
테스트 서버 프론트: http://54.180.29.143
테스트 서버 백: http://54.180.8.184:8010(server), 8011(socket)
개발 서버 13.125.182.116 8090 8091 -> 54.180.29.19 8010 8011
실서버 13.125.31.75 8010 8011 -> 13.125.101.121 8010 8011
*/

export { dockerUrl };
export const baseUrl = `${serverUrl}:${backPort}`;
// export const faceUrl = `${faceServerUrl}:${facePort}`;
export const websocketUrl = `${serverUrl}:${socketPort}`;
export const streamUrl = `${frontUrl}/hls`;
export const pingUrl = `${serverUrl}:${pingPort}`;
