// import axios from 'axios';
// import config from './config';
/* eslint-disable camelcase */
/* eslint-disable no-unused-vars */
import http from './http';
import utils from '../utils';

export default {
  getClassResult({ class_id }) {
    return http.get('/classes/result', {
      params: {
        class_id,
      },
    });
  },
  getMyBoards() {
    return http.get('/classes/myBoards');
  },
  getMyNotices() {
    return http.get('/classes/myNotices');
  },
  getClassLists() {
    return http.get('/classes');
  },
  // 주제 수강 신청 password에 해당하는 view_type(1 = 공개)인 sign_up_end_time이 지나지 않은 강의 가져오기
  getClassListsByBelongingPassword({ pwd }) {
    return http.get(`/classes/belonging_pwd/${pwd}`);
  },
  getClassEnrollPasswordByPassword({ pwd }) {
    return http.get(`/classes/enroll/password/${pwd}`);
  },
  getClassBelongingByCode({ code }) {
    return http.get(`/classes/belonging/${code}`);
  },
  getClassBelongingByClassId({ classId }) {
    return http.get(`/classes/belonging/id/${classId}`);
  },
  // 강의의 password 테이블 데이터를 받아옴)
  getClassPasswordById({ id }) {
    return http.get(`/classes/${id}/password`);
  },
  enrollClassByPassword({ password }) {
    return http.post('/classes/enroll/password', {
      password,
    });
  },
  getClassEnrollPasswordById({ id }) {
    return http.get(`/classes/${id}/enroll/password`);
  },
  // 해당 강의의 password가 존재하면 업데이트 없으면 생성
  postClassEnrollPassword({ classId, password }) {
    return http.post('/classes/create/enroll/password', {
      classId,
      password,
    });
  },
  getNoticeList({ classId, pageNum, type, query }) {
    return http.get(`/classes/${classId}/notice?pageNum=${pageNum}&type=${type}&query=${query}`);
  },
  getNotice({ classId, noticeId }) {
    return http.get(`/classes/${classId}/notice/${noticeId}`);
  },
  postNoticeComment({ classId, noticeId, comment }) {
    return http.post(`/classes/notice_reply`, {
      classId,
      noticeId,
      comment,
    });
  },
  deleteNotice({ noticeId }) {
    return http.delete(`/classes/notice/${noticeId}`);
  },
  deleteNoticeReply({ noticeReplyId }) {
    return http.delete(`/classes/notice_reply/${noticeReplyId}`);
  },
  modifyNotice({ noticeId, name, content, fileGuid }) {
    return http.put(`/classes/notice`, {
      noticeId,
      name,
      content,
      fileGuid
    });
  },
  postNotice({ classId, name, content, fileGuid }) {
    return http.post(`/classes/notice`, {
      classId,
      name,
      content,
      fileGuid,
    });
  },
  getMainClassLists() { // TODO : 필요 없다면 삭제할 것.
    return http.get('/main_classes');
  },
  getMyClassList() { //
    return http.get('/classes/my');
  },
  getAffiliationClassList() { //
    return http.get('/classes/affiliation');
  },
  getClass({ id }) { // 주제 정보 불러오기 (강의, 아이템까지 포함)
    return http.get(`/classes/${id}`);
  },
  getClassOnly({ id }) { // 주제 정보만 불러오기
    return http.get(`/classes/${id}/only`);
  },
  getClassCoverage({ id }) {
    return http.get(`/classes/${id}/coverage`);
  },
  getClassNeedScoring({ id }) {
    return http.get(`/classes/${id}/need-scoring`);
  },
  getClassTotalResult({ id }) {
    return http.get(`/classes/${id}/total-result`);
  },
  getClassTotalResultNew({ id }) {
    return http.get(`/classes/${id}/total-result1`);
  },
  NNgetClassTotalResult({ id, classId }) {
    return http.get(`/classes/${id}/${classId}/total-result`);
  },
  getClassStudentResult({ id }) {
    return http.get(`/classes/${id}/student-result`);
  },
  getClassKeywordRelations({ id }) {
    return http.get(`/classes/${id}/keyword-relations`);
  },
  getMasterUniLists() {
    return http.get('/university/list');
  },
  postClass({ //
    title,
    description,
    intendedLectureNum,
    activeStartDate,
    activeEndDate,
    teacherEmailList,
  }) {
    return http.post('/classes', {
      name: title,
      description,
      intendedLectureNum,
      start_time: activeStartDate,
      end_time: activeEndDate,
      teachers: teacherEmailList ? teacherEmailList : [], // eslint-disable-line
    });
  },
  /*
  NNpostClass({
    title,
    opened,
    summary,
    activeStartDate,
    activeEndDate,
    capacity,
    lecturerDescription,
    description,
  }) {
    return http.post('/classes', {
      name: title,
      opened,
      summary,
      start_time: activeStartDate,
      end_time: activeEndDate,
      capacity,
      lecturer_description: lecturerDescription,
      description,
      teachers: [], // 현재 프론트에서는 공동 강사 입력 없음 23 May 2018
    });
  }, */
  NNpostClass({
    title,
    summary,
    activeStartDate,
    activeEndDate,
    openCheck,
    signUpStartDate,
    signUpEndDate,
    type,
    //password,
    num,
    reqId,
    affiliationId,
    isOpenSpace,
    tempAffiliationId
  }) {      
    if(tempAffiliationId !== undefined) affiliationId = tempAffiliationId;
    return http.post('/classes', {
      name: title,
      summary,
      start_time: activeStartDate,
      end_time: activeEndDate,
      view_type: openCheck,
      sign_up_start_time: signUpStartDate,
      sign_up_end_time: signUpEndDate,
      type,
      //password,
      num,
      req_id: reqId,
      affiliationId,
      isOpenSpace,
      teachers: [], // 현재 프론트에서는 공동 강사 입력 없음 23 May 2018
    });
  },
  getCreatingInfo() {
    return http.get('/classes/creating_info');
  },
  deleteCreatingInfo({
    id,
  }) {
    return http.delete(`/classes/creating_info/${id}`);
  },
  putClass({
    name,
    description,
    intendedLectureNum,
    activeStartDate,
    activeEndDate,
    opened,
    id,
  }) {
    const param = {};
    utils.assignIfNotNil(param, { name });
    utils.assignIfNotNil(param, { description });
    utils.assignIfNotNil(param, { intendedLectureNum });
    utils.assignIfNotNil(param, { start_time: activeStartDate });
    Object.assign(param, { end_time: activeEndDate });
    utils.assignIfNotNil(param, { opened });
    return http.put(`/classes/${id}`, param);
  },
  /* 20200309 이전 코드
  NNputClass({
    id,
    title,
    opened,
    summary,
    activeStartDate,
    activeEndDate,
    capacity,
    lecturerDescription,
    description,
  }) {
    return http.put(`/classes/${id}`, {
      name: title,
      opened,
      summary,
      start_time: activeStartDate,
      end_time: activeEndDate,
      capacity,
      lecturer_description: lecturerDescription,
      description,
    });
  }, */
  NNputClass({
    id,
    title,
    summary,
    activeStartDate,
    activeEndDate,
    openCheck,
    signUpStartDate,
    signUpEndDate,
    num,
    isOpenSpace,
  }) {
    return http.put(`/classes/${id}`, {
      name: title,
      summary,
      start_time: activeStartDate,
      end_time: activeEndDate,
      view_type: openCheck,
      sign_up_start_time: signUpStartDate,
      sign_up_end_time: signUpEndDate,
      num,
      isOpenSpace,
    });
  },
  postTeacher({
    id,
    teacherEmail,
  }) {
    return http.post(`/classes/${id}/teacher/${teacherEmail}`, {});
  },
  deleteTeacher({
    id,
    teacherEmail,
  }) {
    return http.delete(`/classes/${id}/teacher/${teacherEmail}`);
  },
  putScore({ id, score }) {
    const param = {};
    utils.assignIfNotNil(param, { score });
    return http.put(`/student/answer/${id}`, param);
  },
  putHomeworkFeedback({ id, feedback }) {
    const param = {};
    utils.assignIfNotNil(param, { feedback });
    return http.put(`/student/homework/${id}`, param);
  },
  delete({
    id,
  }) {
    return http.delete(`/classes/${id}`);
  },
  getClassUser({
    id,
  }) {
    return http.get(`/classes/${id}/user`);
  },
  postClassUserByTeacher({
    classId,
    userId,
  }) {
    return http.post(`/classes/${classId}/user/teacher`, { userId });
  },
  postClassUser({
    id,
  }) {
    return http.post(`/classes/${id}/user`);
  },
  deleteClassUser({
    classId,
    userId,
  }) {
    return http.delete(`/classes/${classId}/user/${userId}`);
  },
  regradeQuestion({
    questionId,
  }) {
    return http.get(`/student_lecture_logs/re/${questionId}`);
  },

// 행정 관리자 methods
  NNMasterputUni({
    id,
    code,
    name,
    address,
    manager,
    email,
    phone,
  }) {
    return http.put(`/view/uni/${id}`, {
      code,
      name,
      address,
      manager,
      email,
      phone,
    });
  },
  NNMasterpostUni({
    code,
    name,
    address,
    manager,
    email,
    phone,
  }) {
    /*
    return http.post('/register/uni/success', {
      code: code,
      name,
      address,
      manager,
      email,
      phone,
     바로 성공 페이지로 돌아가는 거면(목록명시x) 위의 요소들 필요 없는가?
    });
    */
    return {
      success: true,
    };
  },
  NNMasterputDept({
    choiceUni,
    code,
    name,
    fields,
    manager,
    email,
    phone,
  }) {
    return http.put('/view/dept', {
      choiceUni,
      code,
      name,
      fields,
      manager,
      email,
      phone,
    });
  },
  NNMasterpostDept({
    choiceUni,
    code,
    name,
    fields,
    manager,
    email,
    phone,
  }) {
    return {
      success: true,
    };
  },
  NNMasterputTeacher({
    email,
    password,
    passwordConfirm,
    name,
    sex,
    career,
    birth,
    choiceUni,
    choiceDept,
    address,
    phone,
    account,
    bank,
  }) {
    return http.put('view/teacher', {
      email,
      password,
      passwordConfirm,
      name,
      sex,
      career,
      birth,
      choiceUni,
      choiceDept,
      address,
      phone,
      account,
      bank,
    });
  },
  NNMasterpostTeacher({
    email,
    password,
    passwordConfirm,
    name,
    sex,
    career,
    birth,
    choiceUni,
    choiceDept,
    address,
    phone,
    account,
    bank,
  }) {
    return {
      success: true,
    };
  },
  NNMasterputClass({
    choiceUni,
    choiceDept,
    choiceTeacher,
    code,
    frequency,
    name,
    time,
    classRoom,
    activeStartDate,
    activeEndDate,
    capacity,
    lecturerDescription,
    description,
  }) {
    return http.put('/view/class', {
      choiceUni,
      choiceDept,
      choiceTeacher,
      code,
      frequency,
      name,
      time,
      classRoom,
      activeStartDate,
      activeEndDate,
      capacity,
      lecturerDescription,
      description,
    });
  },
  NNMasterpostClass({
    choiceUni,
    choiceDept,
    choiceTeacher,
    code,
    frequency,
    name,
    time,
    classRoom,
    activeStartDate,
    activeEndDate,
    capacity,
    lecturerDescription,
    description,
  }) {
    return {
      success: true,
    };
  },
  NNMasterputStudent({
    email,
    password,
    passwordConfirm,
    name,
    sex,
    birth,
    choiceUni,
    choiceDept,
    address,
    phone,
    account,
    bank,
  }) {
    return http.put('view/student', {
      email,
      password,
      passwordConfirm,
      name,
      sex,
      birth,
      choiceUni,
      choiceDept,
      address,
      phone,
      account,
      bank,
    });
  },
  NNMasterpostStudent({
    email,
    password,
    passwordConfirm,
    name,
    sex,
    birth,
    choiceUni,
    choiceDept,
    address,
    phone,
    account,
    bank,
  }) {
    return {
      success: true,
    };
  },
  NNMasterputBank({
    code,
    name,
    capacity,
    choiceTeacher,
  }) {
    return (http.put('view/bank'), {
      code,
      name,
      capacity,
      choiceTeacher,
    });
  },
  NNMasterpostBank({
    code,
    name,
    capacity,
    choiceTeacher,
  }) {
    return {
      success: true,
    };
  },
  getLectureResult({ id }) {
    return http.get(`/classes/lectureResult/${id}`);
  },
  getClassLectureResult({ id, id2 }) {
    return http.get(`/classes/classLectureResult/${id}/${id2}`);
  },
  getClassLectureItemResult({
    classId,
    lectureId,
  }) {
    return http.get(`/classes/classLectureItemResult/${classId}/${lectureId}`);
  },
  putClassToHide({
    id,
  }) {
    return http.put(`/classes/hide/${id}`);
  },
  getClassRating({
    classId,
  }) {
    return http.get(`/classes/${classId}/rating`);
  },
  postClassRating({
    classId,
    rating,
  }) {
    return http.post(`/classes/${classId}/${rating}`);
  },
  getClassAffiliation({
    classId,
  }) {
    return http.get(`/classes/affiliation/${classId}`);
  },
  // 2021-03-24 Docker 상태 변경
  postChangeDockerStatus({
    classId,
    dockerStatus,
  }){
    return http.post(`/classes/docker/${classId}`,
    {
      dockerStatus,
    });
  },
  deleteDocker({
    classId,
  }){
    return http.delete(`/classes/dockerDelete/${classId}`);
  },
  createDocker({
    classId,
  }){
    return http.post(`/classes/dockerCreate/${classId}`);
  },
};
