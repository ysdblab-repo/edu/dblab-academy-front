import http from './http';

export default {
  getLectureList({ classId }) {
    return http.get(`/attend/${classId}/0`);
  },
  getLectureAuths({ lectureId }) {
    return http.get(`/attend/lecture/${lectureId}`);
	},
	getLectureSTDAuths({ lectureId }) {
    return http.get(`/attend/lecture/student/${lectureId}`);
	},
	getSTDAuths({ studentId }) {
		return http.get(`attend/student/${studentId}`);
	},
  getAllLectureAuths({ lectureIds }) {
    return http.get(`/attend/all/lecture`, {
      params: {
        lectures: lectureIds,
      },
    });
  },
  // getLectureCheck({ lectureId }) {
  //   return http.get(`/reports/check-lecture/${lectureId}`);
  // },
  // postLectureResult({ lectureId, result, type }) {
  //   return http.post(`reports/lectures/${lectureId}`, {
  //     result,
  //     type,
  //   });
  // },
};
